const machineCode = function (asm) {
    let len = asm.length
    let tokens = []
    let spaces = '\t\n\r '
    let register = 'xyz'
    let digits = '1234567890'
    let letters = 'abcdefghijklmnopqrstuvwxyz'
    let i = 0
    while (i < len) {
        let c = asm[i]
        i += 1
        if (spaces.includes(c)) {
            continue;
        } else if (c === '@') {
            let s = '@'
            while (i < len) {
                if (spaces.includes(asm[i])) {
                    break
                }
                s += asm[i]
                i += 1
            }
            tokens.push(s)
        } else if (digits.includes(c)) {
            let end =  0
            let range = asm.slice(i)
            for (let j = 0; j < range.length; j++) {
                let e = range[j]
                if (!digits.includes(e)) {
                    end = j
                    break
                }
            }
            let n = asm.slice(i-1, i+end)
            i += end
            tokens.push(parseInt(n))
        } else if (register.includes(c)) {
            tokens.push(c)
        } else if (letters.includes(c)) {
            let offset = i
            let f = c
            while (offset < len) {
                if (!letters.includes(asm[offset])) {
                    i = offset + 1
                    tokens.push(f)
                    break
                } else {
                    f += asm[offset]
                    offset += 1
                }
            }
        }
    }
    console.log(tokens.toString())
    return tokens
}

function parseAsm(list) {
    for (let i = 0; i < list.length; i++) {
        let e = list[i];
        if (e[0] === '@') {
            list[i] = parseInt(e[1])
        }
    }
    let d = {
        'x': 16,
        'y': 32,
        'z': 48,
        'w': 64,
        "set": 0,
        "load": 1,
        "add": 2,
        "save": 3,
        'sfr': 7,
        'jump': 8,
        'jumpwhenless': 9,
        'compare': 10,
        'moveleft':11,
        'setleft':12,
        'stop':256,
    }

    for (let i = 0; i < list.length; i++) {
        let e = list[i];
        if (typeof e === "string") {
            list[i] = d[e]
        }
    }
    console.log(list.toString())
    return list.map(e => e.toString(2))
}


function __main() {
    let temp = machineCode(`
        set y 516
        set z 2
        set x 195
        sfr x y
        add y z y
        sfr x y
        jump 128
        jumpwhenless 10
        compare
    `)
    console.log(parseAsm(temp).toString())
}

__main()
