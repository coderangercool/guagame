let mm = Array(65536).fill(0)

function setFonts() {
    const fonts = [
        //. 4-7
        0b11000000,0b11000000,0b00000000,0b00000000,
        //, 8-11
        0b10110000,0b01110000,0b00000000,0b00000000,
        //space 12-15
        0b00000000,0b00000000,0b00000000,0b00000000,
        //$ 16-19
        0b01011100,0b11010110,0b01110100,0b00000000,
        //¥ 20-23
        0b01010110,0b11110000,0b01010110,0b00000000,
        //0 24-27
        0b11111110,0b10000010,0b11111110,0b00000000,
        //1 28-31
        0b10000010,0b11111110,0b10000000,0b00000000,
        //2 32-35
        0b11110010,0b10010010,0b10011110,0b00000000,
        //3 36-39
        0b10010010,0b10010010,0b11111110,0b00000000,
    ]
    for (let i = 4; i < 40; i++) {
        mm[i] = fonts[i-4]
    }
}



log = console.log.bind(console)

ins = {
    "0": {
        name: "set",
        argNum: 2,
        regPos: 1,
    },
    "1": {
        name: "load",
        argNum: 2,
        regPos: 2,
    },
    "2": {
        name: "add",
        argNum: 3,
    },
    "3": {
        name: "save",
        argNum: 2,
        regPos: 1,
    },
    "7": {
        name: 'sfr',
        argNum: 2,
    },
    '8': {
        name: 'jump',
        argNum: 1,
    },
    '9': {
        name: 'jumpwhenless',
        argNum: 1,
    },
    '10': {
        name: 'compare',
        argNum: 0,
    },
    "11": {
        name: 'moveleft',
        argNum:1,
    },
    '12': {
        name: 'setleft',
        argNum: 2
    },
    "256": {
        name: 'stop',
        argNum: 0
    }
}

func = {
    'add': {
        argNum: 3,
    },
    'sfr': {
        argNum: 2,
    },
    'moveleft': {
        argNum: 1
    },
    'setleft': {
        argNum: 2
    }

}

register = {
    "16": 'x',
    "32": 'y',
    "48": 'z',
    "64": 'w',
}

const run = function(memory) {
    /*
    这是一个虚拟机程序

    之前作业的 assembler.py 可以生成一个包含数字的数组
    参数 memory 就是那个数组

    run 函数将 memory 数组视为内存，可以执行这个内存
    你需要用变量来模拟寄存器，模拟程序的执行

    稍后会给出一个用于测试的 memory 例子并更新在 #general
    你现在可以用自己生成的内容来测试

    注意，memory 目前只能支持 256 字节
    因为 pc 寄存器只有 8 位（1 字节）
    */

    const instruction = memory.map(e => parseInt(e, 2).toString())
    log('10进制的',instruction.toString())
    let i  = 0;
    while (i < instruction.length) {
        let t = instruction[i]
        if (t in ins) {
            instruction[i] = ins[t].name
            if (ins[t].regPos) {
                let rIndex = i + ins[t].regPos
                let rValue = instruction[rIndex]
                instruction[rIndex] = register[rValue]
            }
            i = i + ins[t].argNum + 1;
        }
    }
    for (let j = 0; j < instruction.length; j++) {
        let e = instruction[j]
        if (e in func) {
            let argNum = func[e].argNum
            for (let k = 1; k <= argNum; k++) {
                let rIndex = j + k
                let rValue = instruction[rIndex]
                instruction[rIndex] = register[rValue]
            }
        }
    }
    log(instruction.toString())
    let vars = {x:0, y:0, z:0, w:0, pc:1024, bo:0}
    for (let j = 0; j < instruction.length; j++) {
        mm[1024+j] = instruction[j]
    }
    log('instruction#################',mm.slice(1024))
    let count = vars.pc
    while (mm[count] !== 'stop') {
        let e = mm[count]
        if (e === 'set') {
            vars[mm[count+1]] = mm[count+2]
        }
        if (e === 'sfr') {
            let y = parseInt(vars.y)
            let x = parseInt(vars.x)
            mm[y] = x
            log('...mm', mm[y])
        }
        if (e === 'add') {
            let a = parseInt(vars[mm[count+1]])
            let b = parseInt(vars[mm[count+2]])
            vars[mm[count+3]] = a + b
        }
        if (e === 'load') {
            let index = parseInt(mm[count+1])
            let value = parseInt(mm[index]).toString(2)
            log('value', value)
            while (value.length < 8) {
                value = '0' + value
            }
            value = value.split('').reverse().join('')
            vars[mm[count+2]] = value
            log('vars.wcoooooooool', vars.w)
        }
        if (e === 'setleft') {
            let from = mm[count+1]
            let to = mm[count+2]
            let val = parseInt(vars[from].slice(0,1))
            vars[to] = val
        }
        if (e === 'moveleft') {
            let pos = mm[count+1]
            let value = vars[pos]
            value = value.slice(1)
            vars[pos] = value
        }
        count++
        vars.pc = count
    }
    log(mm.slice(512, 1023).toString())
    log(vars)
    draw(mm , vars)
}

function draw(data) {
    let pixelData = data.slice(512, 1023)
    let color = {r:255, g:0, b:0, a:255}
    log('red', color)
    let cvs = document.querySelector('#id-canvas')
    let ctx = cvs.getContext('2d')
    let cvsPixels = ctx.getImageData(0, 0, cvs.width, cvs.height)
    let pixelsArray = cvsPixels.data
    let {r, g, b, a} = color
    for (let j = 0; j < pixelData.length; j++) {
        if (pixelData[j] !== 0) {
            let index = j * 4
            pixelsArray[index] = r
            pixelsArray[index+1] = g
            pixelsArray[index+2] = b
            pixelsArray[index+3] = a
        }
    }
    ctx.putImageData(cvsPixels, 0, 0)
}

function __main() {
    let instructions = `
        set y 512
        set z 32
        load 16 w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        ========================================
        set y 513
        load 17 w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        ========================================
        set y 514
        load 18 w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        ========================================
        set y 515
        load 19 w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        ========================================
        set y 516
        load 32 w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        ========================================
        set y 517
        load 33 w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        ========================================
        set y 518
        load 34 w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        ========================================
        set y 519
        load 35 w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        ========================================
        set y 520
        load 4 w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        ========================================
        set y 521
        load 5 w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        ========================================
        set y 522
        load 6 w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        ========================================
        set y 523
        load 7 w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        ========================================
        set y 524
        load 36 w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        ========================================
        set y 525
        load 37 w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        ========================================
        set y 526
        load 38 w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        ========================================
        set y 527
        load 39 w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        add y z y
        moveleft w
        setleft w x
        sfr x y
        ========================================
        stop
    `
    setFonts()
    const memory = parseAsm(machineCode(instructions))
    log(memory)
    const result = run(memory)
    log(result)
    return result;
}

__main()