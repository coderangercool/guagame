from PIL import Image
import copy
import json
import sys


def grayimage(path):
    # convert('L') 转为灰度图
    # 这样每个像素点就只有一个灰度数据
    img = Image.open(path).convert('L')
    return img


def encode(path1, path2):
    img1 = grayimage(path1)
    img2 = grayimage(path2)
    result = []
    dictionary = {
        'x': -1,
        'y': -1,
        'data': []
    }
    w, h = img1.size
    m, n = w // 8, h // 8
    pixels1 = img1.load()
    pixels2 = img2.load()
    exit_sign = False
    for y in range(n):
        for x in range(m):
            found = False
            if exit_sign is True:
                exit_sign = False
            diff_list = []
            if x == 0 or y == 0:
                d = copy.deepcopy(dictionary)
                i = x * 8
                j = y * 8
                for jj in range(j, j + 8):
                    for ii in range(i, i + 8):
                        d['data'].append(pixels2[ii, jj])
                result.append(d)
            else:
                for yy in [y - 1, y, y + 1]:
                    if yy >= n:
                        break
                    if exit_sign is True:
                        break
                    for xx in [x - 1, x, x + 1]:
                        if xx >= m:
                            break
                        if exit_sign is True:
                            break
                        sump = 0
                        diff = 0
                        xxx = xx * 8
                        yyy = yy * 8
                        ii = x * 8
                        jj = y * 8
                        for j in range(yyy, yyy + 8):
                            if exit_sign is True:
                                break
                            # jj = (j - yyy) + (y * 8)
                            iii = ii
                            for i in range(xxx, xxx + 8):
                                # ii = (i - xxx) + (x * 8)
                                sump += abs(pixels2[iii, jj] - pixels1[i, j])
                                diff += abs(pixels2[iii, jj] - pixels1[i, j]) / 2 + 128
                                iii = iii + 1
                            jj = jj + 1
                        diff_dic = {
                            'x': xxx,
                            'y': yyy,
                            'diff': diff
                        }
                        diff_list.append(diff_dic)
                        if sump < 2:
                            d = copy.deepcopy(dictionary)
                            d['x'] = xxx
                            d['y'] = yyy
                            d['data'] = None
                            result.append(d)
                            exit_sign = True
                            found = True
                            break
                if found is False:
                    # d = copy.deepcopy(dictionary)
                    # i = x * 8
                    # j = y * 8
                    # for jj in range(j, j + 8):
                    #     for ii in range(i, i + 8):
                    #         d['data'].append(pixels2[ii, jj])

                    diff_dict = sorted(diff_list, key=lambda dd: dd['diff'])[0]
                    d = copy.deepcopy(dictionary)
                    d['x'] = diff_dict['x']
                    d['y'] = diff_dict['y']
                    d['data'] = None
                    result.append(d)
    return result


def diff_pic(path1, path2, data, tname):
    img1 = grayimage(path1)
    img2 = grayimage(path2)
    w, h = img1.size
    m, n = w // 8, h // 8
    pixels1 = img1.load()
    pixels2 = img2.load()

    for y in range(n):
        for x in range(m):
            index = y * m + x
            d = data[index]
            p = data[index]['data']
            if d['x'] == -1:
                i = x * 8
                j = y * 8
                for jj in range(j, j + 8):
                    for ii in range(i, i + 8):
                        pidx = (jj - j) * 8 + ii - i
                        pixels1[ii, jj] = p[pidx]
            elif d['x'] > 0:
                i = x * 8
                j = y * 8
                ti = d['x']
                tj = d['y']

                for jj in range(j, j + 8):
                    tti = ti
                    for ii in range(i, i + 8):
                        pixels1[ii, jj] = pixels1[tti, tj]
                        tti = tti + 1
                    tj = tj + 1
    for y in range(h):
        for x in range(w):
            b1 = pixels1[x, y]
            b = pixels2[x, y]
            diff = int(128 + (b1 - b) / 2)
            pixels1[x, y] = diff
    return img1
    # img1.save(tname + '.diff.png')


def decode(path, data, diff_path):
    img = grayimage(path)
    diff_img = grayimage(diff_path)
    w, h = img.size
    m, n = w // 8, h // 8
    pixels = img.load()
    pixels_diff = diff_img.load()
    for y in range(n):
        for x in range(m):
            index = y * m + x
            d = data[index]
            p = data[index]['data']
            if d['x'] == -1:
                i = x * 8
                j = y * 8
                for jj in range(j, j + 8):
                    for ii in range(i, i + 8):
                        pidx = (jj - j) * 8 + ii - i
                        pixels[ii, jj] = p[pidx]
            elif d['x'] > 0:
                i = x * 8
                j = y * 8
                ti = d['x']
                tj = d['y']
                for jj in range(j, j + 8):
                    tti = ti
                    for ii in range(i, i + 8):
                        pixels[ii, jj] = pixels[tti, tj]
                        tti = tti + 1
                    tj = tj + 1
    for y in range(h):
        for x in range(w):
            diff = pixels_diff[x, y]
            b1 = pixels[x, y]
            b = b1 - (diff - 128) * 2
            pixels[x, y] = b
    img.save('decode.png')


#
# def main():
#     path1 = 'big_buck_bunny_07501.png'
#     path2 = 'big_buck_bunny_07502.png'
#     img1 = grayimage(path1)
#     img2 = grayimage(path2)
#
#     # 下面是一段遍历像素并操作的范例
#     # 供参考
#     w, h = img1.size
#     threshold = 128
#     pixels = img1.load()
#     for y in range(h):
#         for x in range(w):
#             if pixels[x, y] < threshold:
#                 pixels[x, y] = 0
#             else:
#                 pixels[x, y] = 255
#     img1.save('保存图片样例.png')


def main():
    if sys.argv[1] == 'encode':
        path1 = sys.argv[2]
        path2 = sys.argv[3]
        tname = sys.argv[3].split('.')[0]
        data = encode(path1, path2)
        diffPng = diff_pic(path1, path2, data, tname)
        diffPng.save(tname + '.diff.png')
        json_data = json.dumps(data)
        file = open(tname + '.vblock', 'w')
        file.write(json_data)
        file.close()

    if sys.argv[1] == 'decode':
        path1 = sys.argv[3]
        path1_name = path1.split('.')[0]
        path1_num = int(path1_name.split('_')[-1])
        pic_name = '_'.join(path1_name.split('_')[0:-1])
        path2_name = pic_name + '_0' + str(path1_num + 1)
        vblock = path2_name + '.vblock'
        diff_png = path2_name + '.diff.png'
        file = open(vblock, 'r')
        content = file.read()
        data = json.loads(content)
        file.close()
        decode(path1, data, diff_png)


if __name__ == '__main__':
    main()
