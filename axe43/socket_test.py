import socket

encoding = 'utf-8'
port = 3000
host = 'localhost'
list = 5


def client_create():
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.connect((host, port))
    sock.close()


def main():
    for i in range(3000):
        client_create()


if __name__ == '__main__':
    main()
