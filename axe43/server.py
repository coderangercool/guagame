import threading
import socket

encoding = 'utf-8'
port = 3000
host = 'localhost'
list = 5


def task(client, address):
    buf = client.recv(2048)
    client.send(buf)
    client.close()


def server_create():
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    sock.bind((host, port))
    sock.listen(list)
    while True:
        client, address = sock.accept()
        thread = threading.Thread(target=task, args=(client, address))
        thread.start()
        thread.join()


if __name__ == '__main__':
    server_create()