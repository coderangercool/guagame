def json_tokens(json_string):
    """
    JSON 格式中我们目前只关注以下 8 种基本元素
    {
    }
    字符串（字符串中不包含特殊符号，不用支持转义符）
    :
    ,
    整数
    [
    ]

   json_string 是只包含以上元素的 JSON 格式字符串（也包括空白符号）
    返回 token 的列表
    例如对于以下字符串
    {
        "name": "gua",
        "height": 169
    }
    将返回以下数据
    ["{", "name", ":", "gua", ",", "height", ":", "169", "}"]

   提示：
    用循环去读取字符串
    用下标来标注当前读取的位置
    根据不同的情况来读取不同的数据并存储
    """
    cha = '{}:,[]'
    s = json_string.split('"')
    res = []
    for l in s:
        res += (l.split())
    result = []
    for t in res:
        for a in t:
            if (a in cha and len(t) > 1):
                x, t = t.split(a)
                if x != '':
                    result.append(x)
                result.append(a)
        if t != '':
            result.append(t)
    return result

def ensure(condition, message):
    if not condition:
        print("测试失败", message)


def testJsonTokens():
    ensure(json_tokens('{ "name": "gua","height": 169 }') == ["{", "name", ":", "gua", ",", "height", ":", "169", "}"], 'test json token 1')
    ensure(json_tokens('{ "name": "gua","height": "169","cool": 123 }') == ["{", "name", ":", "gua", ",", "height", ":", "169", ",", "cool", ":", "123", "}"], 'test json token 2')
    ensure(json_tokens('{"name": "123gua","height": "169"}') == ["{", "name", ":", "123gua", ",", "height", ":", "169", "}"], 'test json token 3')


def __main():
    testJsonTokens()

__main()