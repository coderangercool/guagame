#include <stdio.h>
#include <stdbool.h>


#include "GuaTest.h"
#include "GuaHashTable.h"


//void
//testGuaListLength(void) {
//    type a1[] = {1, 2, 3};
//    int n1 = 3;
//    GuaList *l1 = GuaListCreate(a1, n1);
//    ensure(GuaListLength(l1) == n1, "test list length 1");
//
//    type a2[] = {};
//    int n2 = 0;
//    GuaList *l2 = GuaListCreate(a2, n2);
//    ensure(GuaListLength(l2) == n2, "test list length 2");
//}
//
//void
//testGuaStackCreate(void) {
//    GuaStack *s = GuaStackCreate();
//    GuaStackPush(s, 1);
//    GuaStackPush(s, 2);
//    GuaStackLog(s);
//    GuaStackPop(s);
//    GuaStackLog(s);
//}

void
testGuaHashTableHas(void) {
    GuaHashTable *hashtable = GuaHashTableCreate();
    char *index1 = "what is cool";
    int val1 = 123;
    char *index2 = "that is good";

    GuaHashTableSet(hashtable, index1, val1);

    
    ensure(GuaHashTableHas(hashtable, index1) == true, "test hashtable has 1");
    ensure(GuaHashTableHas(hashtable, index2) == false, "test hashtable has 2");
}


void
testGuaHashTable(void) {
    GuaHashTable *hashtable = GuaHashTableCreate();
    char *index1 = "what is cool";
    int val1 = 123;
    char *index2 = "that is good";
    int val2 = 321;
    GuaHashTableSet(hashtable, index1, val1);
    GuaHashTableSet(hashtable, index2, val2);
    
    int result1 = GuaHashTableGet(hashtable, index1);
    int result2 = GuaHashTableGet(hashtable, index2);
    
    ensure(result1 == val1, "test hashtable 1");
    ensure(result2 == val2, "test hashtable 2");
}

int
main(int argc, const char * argv[]) {
    testGuaHashTableHas();
    testGuaHashTable();
    return 0;
}
