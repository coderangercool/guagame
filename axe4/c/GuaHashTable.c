//
//  GuaHashTable.c
//  axe
//
//  Created by scode on 2017/10/12.
//  Copyright © 2017年 scode. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>

#include "GuaHashTable.h"

#define hashLen 100

struct GuaNodeStruct {
    char *key;
    int value;
    int isNull;
};

GuaHashTable hashTable[hashLen];

GuaHashTable *
GuaHashTableCreate(void) {
    int i;
    for (i = 0; i < hashLen; i++) {
        hashTable[i].isNull = 1;
    }
    return hashTable;
}

int GuaHashAddress(const char *key) {
    return *key % 97;
}

void
GuaHashTableSet(GuaHashTable *table, const char *key, int value) {
    int address = GuaHashAddress(key);
    if (GuaHashTableHas(table, key) == false) {
        hashTable[address].value = value;
        hashTable[address].isNull = 0;
    } else {
        while (hashTable[address].isNull == 0 && address < hashLen) {
            address++;
        }
        if (address == hashLen) {
            return;
        }
        hashTable[address].value = value;
        hashTable[address].isNull = 0;
    }
}

// 检查 hashtable 中是否存在这个 key
bool
GuaHashTableHas(GuaHashTable *table, const char *key) {
    int address = GuaHashAddress(key);
    if (hashTable[address].isNull == 1) {
        return false;
    } else {
        return true;
    }
}

// 返回 hashtable 中 key 对应的值, 不考虑 key 不存在的情况, 用户应该用 GuaHashTableHas 自行检查是否存在
int
GuaHashTableGet(GuaHashTable *table, const char *key) {
    int address = GuaHashAddress(key);
    return hashTable[address].value;
}

// 销毁一个 hashtable
void
GuaHashTableRemove(GuaHashTable *table) {
    free(table);
    table = NULL;
}
