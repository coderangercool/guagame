# 作业 8
#
# 作业截止时间：
# 周日（10.22 日）18:00
#
#
# 交作业方式（务必注意）：
# 在项目中新建 axe8 目录
# 在 axe8 目录中新建 py 这个目录
# py 中放本次作业的文件
# 本次作业文件名为 axe8.py
#
#
# 有什么去 #question 频道按规范提问
#
#
# 使用 Python 实现
# 实现 json_tokens 函数
# json_string 是一个 JSON 格式字符串

def json_tokens(json_string):
    """
    在作业 7 的基础上增加 2 个功能
    1，把数字识别为数字
    2，字符串中以转义符形式支持双引号，例如 "a\"b" 会被识别为 a"b
    """
    cha = '{}:,[]'
    res = []
    tempNum = ''
    tempStr = ''
    for i, e in enumerate(json_string):
        if e in cha:
            if tempNum:
                res.append(int(tempNum))
                tempNum = ''
            if tempStr:
                res.append(tempStr)
                tempStr = ''
            res.append(e)
        if ord(e) in range(48, 58):
            tempNum += e
        if ord(e) == 92:
            if json_string[i+1] == '"':
                tempStr += '"'
        if ord(e) in range(65, 91) or ord(e) in range(97, 123):
            tempStr += e
    return res






# 实现下面的函数
def parsed_json(tokens):
    """
    tokens 是一个包含各种 JSON token 的数组（ json_tokens 的返回值）
    返回解析后的 字典
    例如
    ["{", "name", ":", "gua", "}"]
    会返回如下字典
    {
        'name': 'gua',
    }

    需要支持字典 数组 字符串 数字
    不要求支持嵌套字典和嵌套数组
    """
    if tokens[0] == '{':
        return parseDict(tokens)
    if tokens[0] == '[':
        tLen = len(tokens)
        t = tokens[1:(tLen - 1)]
        start = 0
        temp = []
        for i, e in enumerate(t):
            if e == ',' and t[i-1] == '}' and t[i+1] == '{':
                end = i
                l = t[start:end]
                temp.append(list(l))
                start = i + 1
        temp.append(list(t[start:]))
        res = []
        for t in temp:
            res.append(parseDict(t))
        return res

def parseDict(tokens):
    tLen = len(tokens)
    t = tokens[1:(tLen - 1)]
    temp = []
    start = 0
    for i, e in enumerate(t):
        if e == ',':
            end = i
            l = t[start:end]
            temp.append(list(l))
            start = i + 1
    temp.append(list(t[start:]))
    d = dict()
    for l in temp:
        l.pop(1)
        d[l[0]] = l[1]
    return d






def ensure(condition, message):
    if not condition:
        print("测试失败", message)


def testJsonTokens():
    ensure(json_tokens('{ "name": "gu\\"a","height": 169 }') == ["{", "name", ":", 'gu"a', ",", "height", ":", 169, "}"], 'test json token 1')
    ensure(json_tokens('{ "name": "gua","height": 169,"cool": 123 }') == ["{", "name", ":", "gua", ",", "height", ":", 169, ",", "cool", ":", 123, "}"], 'test json token 2')
    ensure(json_tokens('{"name": 123,"height": 169}') == ["{", "name", ":", 123, ",", "height", ":", 169, "}"], 'test json token 3')


def testParsedJson():
    ensure(parsed_json(["{", "name", ":", "gua", ",", "height", ":", 169, ",", "cool", ":", 123, "}"]) == {'name': 'gua', 'height': 169, 'cool': 123}, 'test parsed json 1')
    ensure(parsed_json(["[","{", "name", ":", 123, ",", "height", ":", 169, "}",",","{", "name", ":", "gua", ",", "height", ":", 169, ",", "cool", ":", 123, "}","]"]) == [{'name': 123, 'height': 169}, {'name': 'gua', 'height': 169, 'cool': 123}], 'test parsed json 2')
    ensure(parsed_json(["{", "name", ":", 123, ",", "height", ":", 169, "}"]) == {'name': 123, 'height': 169}, 'test parsed json 3')

def __main():
    testJsonTokens()
    testParsedJson()

__main()