import video3
import os
import sys
import json
from PIL import Image


def grayimage(path):
    # convert('L') 转为灰度图
    # 这样每个像素点就只有一个灰度数据
    img = Image.open(path).convert('L')
    return img


def encode(folder, vbd_folder, opath):
    file_num = len([name for name in os.listdir(folder) if os.path.isfile(os.path.join(folder, name))])
    first_frame = Image.open(folder + '/' + os.listdir(folder)[0]).convert('L')
    w, h = first_frame.size
    file = open(opath, 'w')
    file.write('uipn')
    file.write('1.0\n')
    file.write(str(file_num))
    file.write(str(w))
    file.write(str(h))

    file.write(str(w * h))
    pixels_first = first_frame.load()
    for y in range(h):
        for x in range(w):
            file.write(str(pixels_first[x, y]))

    list_diff_png = [name for name in os.listdir(vbd_folder) if os.path.splitext(name)[1] == '.png']
    list_vblock = [name for name in os.listdir(vbd_folder) if os.path.splitext(name)[1] == '.vblock']

    for path in list_diff_png:
        path = vbd_folder + '/' + path
        img = grayimage(path)
        w, h = img.size
        file.write(str(w * h))
        pixels = img.load()
        for y in range(h):
            for x in range(w):
                file.write(str(pixels[x, y]))

    for path in list_vblock:
        path = vbd_folder + '/' + path
        file_vb = open(path, 'r')
        content = file_vb.read()
        data_vb = json.loads(content)
        file_vb.close()
        for d in data_vb:
            x = d['x']
            y = d['y']
            file.write(str(x))
            file.write(str(y))

    file.close()


def main():
    video3.vblock_diff()
    folder = sys.argv[2]
    opath = sys.argv[3] + '.uipn'
    vbd_folder = 'vblock_diff'
    encode(folder, vbd_folder, opath)


if __name__ == '__main__':
    main()
