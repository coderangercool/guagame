import video2
import json
import os


def vblock_diff():
    os.makedirs('vblock_diff')
    folder = 'vblock_diff/'
    path_base = 'images/big_buck_bunny_0'
    for n in range(7501, 7546):
        path1 = path_base + str(n) + '.png'
        path2 = path_base + str(n + 1) + '.png'
        tname = path2.split('/')[1].split('.')[0]
        data = video2.encode(path1, path2)
        json_data = json.dumps(data)
        file = open(folder + tname + '.vblock', 'w')
        file.write(json_data)
        file.close()
        diffPng = video2.diff_pic(path1, path2, data, tname)
        diffPng.save(folder + tname + '.diff.png')


def main():
    vblock_diff()


if __name__ == '__main__':
    main()
