#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<stdbool.h>
#include<sys/socket.h>
#include<arpa/inet.h>
#include<unistd.h>
#include<pthread.h>
#include<semaphore.h>

static sem_t sem;
static unsigned short port = 3000;

void
response(int socketFile) {
    sem_wait(&sem);
    int s = socketFile;
    char *message = "connection default response\n";
    write(s , message , strlen(message));
    sem_post(&sem);
}

int
openSocket(unsigned short port) {
    sem_wait(&sem);
    int s = socket(AF_INET, SOCK_STREAM, 0);
    // 消除端口占用错误
    const int option = 1;
    setsockopt(s, SOL_SOCKET, SO_REUSEADDR, (const void *)&option , sizeof(int));
    //
    struct sockaddr_in serveraddr;
    serveraddr.sin_family = AF_INET;
    serveraddr.sin_addr.s_addr = htonl(INADDR_ANY);
    serveraddr.sin_port = htons(port);
    //
    bind(s, (struct sockaddr *)&serveraddr, sizeof(serveraddr));
    listen(s, 5);
    //
    printf("listening at port %d\n", port);
    sem_post(&sem);
    return s;
}

void *
runSocket() {
//    unsigned short port = 3000;
    sem_wait(&sem);
    unsigned short newPort = port + 1;
    int s = openSocket(newPort);
    
    struct sockaddr_in client;
    int size = sizeof(struct sockaddr_in);
    sem_post(&sem);
    while(true) {
        int client = accept(s, (struct sockaddr *)&client, (socklen_t*)&size);
        printf("accept and process\n");
        response(client);
    }
    return NULL;
}

int
main(int argc, const char *argv[]) {
    int n = 10;
    pthread_t tid[n];
    for (int i = 0; i < n; i++) {
        pthread_create(&tid[i], NULL, runSocket, NULL);
    }
    for (int i = 0; i < n; i++) {
        pthread_join(tid[i], NULL);
    }
    return 0;
}

