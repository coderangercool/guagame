import sys
import os

def LZSS_encode(path):
    file = open(path, 'r')
    message = file.read()
    filename = file.name.split('.')[0]
    file.close()
    result = []

    tree_dict, m_len, i = {}, len(message), 0
    while i < m_len:
        # case I
        if message[i] not in tree_dict.keys():
            result.append((0, message[i]))
            tree_dict[message[i]] = len(tree_dict) + 1
            i += 1
        # case III
        elif i == m_len - 1:
            result.append((tree_dict.get(message[i]), ''))
            i += 1
        else:
            for j in range(i + 1, m_len):
                # case II
                if message[i:j + 1] not in tree_dict.keys():
                    result.append((tree_dict.get(message[i:j]), message[j]))
                    tree_dict[message[i:j + 1]] = len(tree_dict) + 1
                    i = j + 1
                    break
                # case III
                elif j == m_len - 1:
                    result.append((tree_dict.get(message[i:j + 1]), ''))
                    i = j + 1

    result = [str(x) for x in result]
    result = ' '.join(result)

    file_w = open(filename + '.lzss', 'w')
    file_w.write(result)
    file_w.close()

def LZSS_decode(src, target):
    dictionary = {i: chr(i) for i in range(97, 123)}
    last = 256
    file = open(src, 'r')
    code = file.read()
    code = code.split(' ')
    print(code)
    code = [tuple(x) for x in code]
    print(code)
    file.close()

    unpacked, tree_dict = '', {}
    for index, ch in code:
        if index == 0:
            unpacked += ch
            tree_dict[len(tree_dict) + 1] = ch
        else:
            term = tree_dict.get(index) + ch
            unpacked += term
            tree_dict[len(tree_dict) + 1] = term

    file_w = open(target, 'w')
    file_w.write(unpacked)
    file_w.close()

def __main():
    if len(sys.argv) < 3:
        LZSS_encode(sys.argv[1])
    # elif len(sys.argv) >= 3:
        # LZSS_decode(sys.argv[1], sys.argv[2])

if __name__ == '__main__':
    __main()