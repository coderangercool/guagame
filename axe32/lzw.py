import sys
import os

def LZW_encode(path):
    file = open(path, 'r')
    string = file.read()
    filename = file.name.split('.')[0]
    file.close()
    dictionary = {chr(i): i for i in range(0, 128)}
    last = 256
    p = ""
    result = []

    for c in string:
        pc = p + c
        if pc in dictionary:
            p = pc
        else:
            result.append(dictionary[p])
            dictionary[pc] = last
            last += 1
            p = c

    if p != '':
        result.append(dictionary[p])

    result = [str(x) for x in result]
    result = ' '.join(result)

    file_w = open(filename + '.lzw', 'w')
    file_w.write(result)
    file_w.close()

def LZW_decode(src, target):
    dictionary = {i: chr(i) for i in range(0, 128)}
    last = 256
    file = open(src, 'r')
    code = file.read()
    code = code.split(' ')
    code = [int(x) for x in code]
    file.close()

    result = []
    p = code.pop(0)
    result.append(dictionary[p])

    for c in code:
        if c in dictionary:
            entry = dictionary[c]
        result.append(entry)
        dictionary[last] = dictionary[p] + entry[0]
        last += 1
        p = c

    file_w = open(target, 'w')
    file_w.write(''.join(result))
    file_w.close()


def __main():
    if len(sys.argv) < 3:
        LZW_encode(sys.argv[1])
    elif len(sys.argv) >= 3:
        LZW_decode(sys.argv[1], sys.argv[2])

if __name__ == '__main__':
    __main()