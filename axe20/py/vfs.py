'''
本次作业使用的编码如下（guacoding3）
0      空
1-9    暂时保留
10     .
11     ,
12     空格
13     $
14     ￥
15-24  0-9
25     A
...
自己算  Z


根据课上讲的文件系统原理，文件系统可看作是对一块存储空间的使用划分方案
实现 guafs，guafs 说明如下

0-4     GUAFS (guacoding3编码)
5-9     填 0，暂时不使用这 5 个字节
10      根目录的文件信息（文件信息的描述在下方，一共 19 字节）


文件信息，19字节
10-19文件名                 10 字节，第一个字节表示文件名的字符串长度
20文件类型               1 字节，0 表示文件，1 表示目录
2122文件长度（目录子文件数量）2 字节
2324父目录地址              2 字节，0 表示没有父目录（只有根目录 / 是 0）
2526同目录下一个文件的地址    2 字节（只支持 64K 容量的硬盘），0 表示没有下一个文件了
2728文件内容开始的地址       2 字节，如果文件类型是目录则表示第一个子文件的文件信息地址

文件内容，大小不定
2930文件信息地址            2 字节，反向指向文件信息


实现下面的类
主要注意的是，  guavfs 中存储的编码是 guacoding3
这个类返回的编码都是 UTF-8
'''
guacoding3 = {
    ".": 10,
    ',': 11,
    ' ': 12,
    '$': 13,
    '¥': 14,
    "~": 51
}


def load_file(path):
    with open(path) as f:
        return f.read()

class GuaVFS():
    def __init__(self, path):
        self.memory = load_file(path).split()
        self.root = self.memory[10:31]
        self.dirs = {'~': 10,}
        self.files = {}

    def make_dir(self, path):
        """ 创建目录, path 中不存在的目录都会被创建 """
        dirs = path.split('/')
        for d in dirs:
            p = [x for x in d]
            newD = [x for x in self.root]
            newD[0] = len(p)
            # keys = [k for k in self.dirs]
            # lastKey = keys[-1]
            # lastFileIndex = self.dirs[lastKey]
            # if lastFileIndex:
            #     newD[15] = len(self.memory) + 1
            for i, n in enumerate(d):
                newD[i+1] = d[i]
            self.dirs[d] = (len(self.memory) + 1)
            newD[16] = len(self.memory) + len(newD) + 1
            newD[20] = len(self.memory) + 1
            self.memory += newD
        print(self.dirs)

    def remove_path(self, path):
        """ 删除路径, 如果参数是个目录则递归删除 """
        dirs = path.split('/')
        target = dirs[-1]
        if not ('.' in target):
            index = self.dirs[target]
            self.memory = self.memory[0:index]

    def list(self, path):
        """ 返回 path 下所有的子文件，以列表形式 """
        list = []
        keys = [key for key in self.dirs]
        for i, key in enumerate(keys):
            item = list[i:i+1]
            list.append(item)
            list.pop()
        return list

    def write(self, path, content):
        """ 把 content 写入 path 中 """
        dirs = path.split('/')
        dir = dirs[-2]
        filename = dirs[-1]
        newF = [x for x in self.root]
        newF[0] = len(filename)
        newF[14] = self.dirs[dir]
        for i, n in enumerate(filename):
            newF[i + 1] = filename[i]
        self.files[filename] = len(self.memory) + 1
        newF[16] = len(self.memory) + len(newF) + 1
        newF[18] = len(self.memory) + 1
        c = [x for x in content]
        newF[12] = len(c)
        self.memory += newF
        self.memory += c

    def read(self, path):
        """ 返回 path 中给定的这个文件的内容 """
        dirs = path.split('/')
        target = dirs[-1]
        index = self.files[target]
        length = self.memory[index+12]
        return self.memory[index:(index+int(length))]


def __main():
    path = 'disk.guavfs'
    vfs = GuaVFS(path)
    vfs.make_dir('~/cool/nice')
    vfs.remove_path('~/cool/nice')
    vfs.list('~/cool')
    vfs.write('~/cool/nice/cool.txt', 'good nice content')
    vfs.read('~/cool/nice/cool.txt')

__main()
