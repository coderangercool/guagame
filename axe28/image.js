class GuaImage extends GuaObject{
    constructor(string) {
        super()
        this.w = 0
        this.h = 0
        this.data = []
        this.setup(string)
    }

    setup(string) {
        let file = string.split('\n').slice(2)
        this.w = Number(file[0])
        this.h = Number(file[1])
        file = file.slice(2)
        file = file.reduce((arr, line) => {
            let list = line.split(' ')
            return arr.concat(list)
        }, [])
        file = file.map((num) => {
            let color = []
            let num2 = Number(num).toString(2)
            if (num2.length < 32) {
                num2 = '0'.repeat(32 - num2.length) + num2
            }
            for (let i = 0; i < 32; i += 8) {
                let n = num2.slice(i, i + 8)
                n = parseInt(n, 2)
                color.push(n)
            }
            return color
        })
        // console.table(file)
        this.data = file
    }
}