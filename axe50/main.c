#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include <SDL2/SDL.h>
#include <SDL2_ttf/SDL_ttf.h>

#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"

#include "button.h"

static SDL_Window *window;
static SDL_Renderer *renderer;
static SDL_Rect *rect;

static char text[100];
static char *composition;
static Sint32 cursor;
static Sint32 selection_len;

int
LuaDrawLine(lua_State *L) {
    int x1 = lua_tonumber(L, 1);
    int y1 = lua_tonumber(L, 2);
    int x2 = lua_tonumber(L, 3);
    int y2 = lua_tonumber(L, 4);
    
    SDL_RenderDrawLine(renderer, x1, y1, x2, y2);
    return 1;
}

int
initsdl() {
    // 初始化 SDL
    SDL_Init(SDL_INIT_VIDEO);
    int width = 800;
    int height = 600;
    // 创建窗口
    // 窗口标题 窗口x 窗口y 宽 高 额外参数
    window = SDL_CreateWindow(
                              "SDL window",
                              SDL_WINDOWPOS_UNDEFINED,
                              SDL_WINDOWPOS_UNDEFINED,+
                              width,
                              height,
                              SDL_WINDOW_RESIZABLE
                              );
    
    // 创建渲染层 文档如下
    // http://wiki.libsdl.org/SDL_CreateRenderer?highlight=%28SDL_CreateRenderer%29
    renderer = SDL_CreateRenderer(
                                  window,
                                  -1,
                                  SDL_RENDERER_ACCELERATED
                                  );
    
    return 0;
}

void
closeSDL() {
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
}

void
updateInput() {
    // 事件套路，参考我 github 的渲染器相关代码
    SDL_Event event;
    while(SDL_PollEvent(&event)) {
        switch(event.type) {
            case SDL_KEYDOWN:
                break;
            case SDL_QUIT:
                // 退出，点击关闭窗口按钮的事件
                closeSDL();
                exit(0);
                break;
            case SDL_TEXTINPUT:
                strcat(text, event.text.text);
                break;
            case SDL_TEXTEDITING:
                composition = event.edit.text;
                cursor = event.edit.start;
                selection_len = event.edit.length;
                break;
        }
    }
}

SDL_Rect*
GuaInputAddView(SDL_Renderer* renderer, GuaButton* button) {
    SDL_Rect *srcrect = malloc(sizeof(SDL_Rect));
    srcrect->x = button->x;
    srcrect->y = button->y;
    srcrect->w = button->w;
    srcrect->h = button->h;
    
    SDL_RenderFillRect(renderer, srcrect);
    return srcrect;
}

int
draw(lua_State *L) {
    // 设置背景颜色并清除屏幕
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderClear(renderer);
    
    // 设置画笔颜色
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    
    //画Input
    GuaButton* inputBox = GuaButtonNew(100, 100, 300, 50);
    rect = GuaInputAddView(renderer, inputBox);

    return 0;
}

SDL_Texture *
GuaTextRenderTexture(SDL_Renderer *renderer, TTF_Font *font, const char *text, SDL_Color color) {
    // 用 TTF_RenderUNICODE_Solid 可以生成汉字字体
    // 不过我们用的字体只有英文字体
    SDL_Surface *surface = TTF_RenderText_Solid(font, text, color);
    SDL_Texture *texture = SDL_CreateTextureFromSurface(renderer, surface);
    SDL_FreeSurface(surface);
    
    return texture;
}

void
GuaTextSetPosition(SDL_Texture *texture, int x, int y, SDL_Rect *rect) {
    SDL_QueryTexture(texture, NULL, NULL, &rect->w, &rect->h);
    rect->x = x;
    rect->y = y;
    // printf("GuaTextSetPosition: %d %d\n", rect->w, rect->h);
}

int
main(int argc, char *argv[]) {
    lua_State *L = luaL_newstate();
    luaL_openlibs(L);

    lua_register(L, "drawLine", LuaDrawLine);

    initsdl();


    // 初始化字体
    TTF_Init();
    const char *fontPath = "OpenSans-Regular.ttf";
    // 打开字体 参数是 fontpath and fontsize
    TTF_Font *font = TTF_OpenFont(fontPath, 34);

    // 生成字体图片并设置图片座标
    SDL_Rect size;
    SDL_Color color = {122, 123, 166, 155,};
    SDL_StartTextInput();

    while(true) {
        // 更新输入
        updateInput();

        // 画图
        draw(L);
        


        // 画文字 注意参数
        SDL_Texture *textTexture = GuaTextRenderTexture(renderer, font, text, color);
        GuaTextSetPosition(textTexture, 100, 100, rect);

        SDL_RenderCopy(renderer, textTexture, NULL, rect);
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
        int nx = rect->x + 20 * selection_len;
        int ny = rect->y;
        int nyb = ny + 50;
        SDL_RenderDrawLine(renderer, nx, ny, nx, nyb);

        // 显示
        SDL_RenderPresent(renderer);
    }

    lua_close(L);

    // 释放字体资源, 这是演示, 实际上代码执行不到这里, 前面是个 while true
    TTF_Quit();

    return 0;
}


