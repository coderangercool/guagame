#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <SDL2/SDL.h>

#include "button.h"

GuaButton*
GuaButtonNew(int posX, int posY, int width, int height) {
    GuaButton* btn = malloc(sizeof(GuaButton));
    btn->x = posX;
    btn->y = posY;
    btn->w = width;
    btn->h = height;
    return btn;
}

int
GuaButtonSetAction(GuaButton* button, void* actionClick) {
    button->action = actionClick;
    return 1;
}


