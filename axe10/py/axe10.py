"""
{
    "{": "gua",
    "height": 169
}

tokenizer
状态机



@SET      ABC       123


["{", "name", ":", "gua", ",", "height", ":", 169, "}"]

enum
枚举

枚举是一个自定义的类型, 用来表示特定的值域/类型
web 前端中的事件名, 就应该是枚举

枚举的好处如下
1, 值检查

button.addEventListener('c1ick', function() {})
"""

from enum import Enum


class Type(Enum):
    auto = 0            # auto 就是 6 个单字符符号, 用来方便写代码的
    colon = 1           # :
    comma = 2           # ,
    braceLeft = 3       # {
    braceRight = 4      # }
    bracketLeft = 5     # [
    bracketRight = 6    # ]
    number = 7          # 169
    string = 8          # "name"
    btrue = 9           # True
    bfalse = 10         # False
    bnull = 11          # Null

class Token(object):
    def __init__(self, token_type, token_value):
        super(Token, self).__init__()
        # 用表驱动法处理 if
        d = {
            ':': Type.colon,
            ',': Type.comma,
            '{': Type.braceLeft,
            '}': Type.braceRight,
            '[': Type.bracketLeft,
            ']': Type.bracketRight,
        }
        if token_type == Type.auto:
            self.type = d[token_value]
        else:
            self.type = token_type
        self.value = token_value

    def __repr__(self):
        return '({})'.format(self.value)


def string_end(code, index):
    """
    code = "abc"
    index = 1
    """
    s = ''
    offset = index
    trans = {
        '\"': '\"',
        '\t': '\t',
        '\n': '\n',
        '\\': '\\',
    }
    while offset < len(code):
        c = code[offset]
        if c == '"':
            # 找到了字符串的结尾
            # s = code[index:offset]
            return (s, offset)
        elif c == '\\':
            # 处理转义符, 现在只支持 \"
            # if code[offset+1] == '"':
            #     s += '"'
            #     offset += 2
            (s, offset) = transformToken(s, code, offset , trans)
            # else:
            #     # 这是一个错误, 非法转义符
            #     pass
        else:
            s += c
            offset += 1
    # 程序出错, 没有找到反引号 "
    pass

def transformToken(s, code, offset, trans):
    key = code[offset+1]
    if key in trans:
        s += trans[key]
        offset += 2
        return (s, offset)
    else:
        raise Exception('not supported token')




def json_tokens(code):
    length = len(code)
    tokens = []
    spaces = '\r'
    digits = '1234567890'
    # 当前下标
    i = 0
    while i < length:
        # 先看看当前应该处理啥
        c = code[i]
        i += 1
        if c in spaces:
            # 空白符号要跳过, space tab return
            continue
        elif c in ':,{}[]':
            # 处理 6 种单个符号
            t = Token(Type.auto, c)
            tokens.append(t)
        elif c == '"':
            # 处理字符串
            s, offset = string_end(code, i)
            i = offset + 1
            # print('i, offset', i, offset, s, code[offset])
            t = Token(Type.string, s)
            tokens.append(t)
        elif c in digits:
            # 处理数字, 现在不支持小数和负数
            end = 0
            for offset, char in enumerate(code[i:]):
                if char not in digits:
                    end = offset
                    break
            n = code[i-1:i+end]
            i += end
            t = Token(Type.number, int(n))
            tokens.append(t)
        elif c == 't':
            t = Token(Type.btrue, True)
            i += 3
            tokens.append(t)
        elif c == 'f':
            i += 3
            t = Token(Type.bfalse, False)
            tokens.append(t)
        elif c == 'n':
            i += 3
            t = Token(Type.bnull, None)
            tokens.append(t)
        else:
            # 出错了
            pass
    result = [t.value for t in tokens]
    return result


# def test_string_end():
#     code = '{ "name": 123 }'
#     pass

# true false null 更多转义符
# 解析嵌套的 JSON 格式字符串


def parsed_json(tokens):
    """
    tokens 是一个包含各种 JSON token 的数组（ json_tokens 的返回值）
    返回解析后的 字典
    例如
    ["{", "name", ":", "gua", "}"]
    会返回如下字典
    {
        'name': 'gua',
    }

    需要支持字典 数组 字符串 数字
    不要求支持嵌套字典和嵌套数组
    """
    if tokens[0] == '{':
        return parseDict(tokens)
    if tokens[0] == '[':
        return parseList(tokens)


def parseList(tokens):
    tLen = len(tokens)
    t = tokens[1:(tLen - 1)]
    start = 0
    temp = []
    for i, e in enumerate(t):
        if e == ',' and t[i - 1] == '}' and t[i + 1] == '{':
            end = i
            l = t[start:end]
            temp.append(list(l))
            start = i + 1
    # temp.append(list(t[start:]))
    if len(t[start:]) > 0:
        temp.append(list(t[start:]))
    res = []
    for t in temp:
        res.append(parseDict(t))
    return res

def parseDict(tokens):
    tLen = len(tokens)
    t = tokens[1:(tLen - 1)]
    temp = []
    start = 0
    for i, e in enumerate(t):
        if e == ',':
            end = i
            l = t[start:end]
            temp.append(list(l))
            start = i + 1
    if len(t[start:]) > 0:
        temp.append(list(t[start:]))
    d = dict()
    for l in temp:
        l.pop(1)
        # print('l', l)
        if str(l[1])[0] == '{':
            result = parseDict(l[1:])
        elif str(l[1])[0] == '[':
            result = parseList(l[1:])
        else:
            result = l[1]
        d[l[0]] = result
    return d


def ensure(condition, message):
    if not condition:
        print("测试失败", message)


def main():
    code1 = """
    {
        "na\\\"me": "\\\tgua",
        "height": 169,
        "cool": true
    }
    """
    res1 = json_tokens(code1)

    code2 = """
    {
        "na\\\"me": "\\\\gua",
        "height": 169,
        "cool": false,
        "contain": {
            "abc": 123
        }
    }
    """
    res2 = json_tokens(code2)

    code3 = """
    {
        "na\\\"me": "\\\ngua",
        "height": 169,
        "cool": null
    }
    """
    res3 = json_tokens(code3)

    ensure(res1 == ['{', 'na"me', ':', '\tgua', ',', 'height', ':', 169, ',', 'cool', ':', True, '}'], "test json token 1")
    ensure(res2 == ['{', 'na"me', ':', '\\gua', ',', 'height', ':', 169, ',', 'cool', ':', False, ',', 'contain',':','{','abc',':',123,'}','}'], "test json token 2")
    ensure(res3 == ['{', 'na"me', ':', '\ngua', ',', 'height', ':', 169, ',', 'cool', ':', None, '}'], "test json token 3")
    # print(parsed_json(res2))
    ensure(parsed_json(res1) == {'na"me': "\tgua","height": 169,"cool": True}, "test parse token 1")
    ensure(parsed_json(res2) == {'na"me': "\\gua","height": 169,"cool": False,"contain": {"abc": 123}}, "test parse token 2")

if __name__ == '__main__':
    main()

