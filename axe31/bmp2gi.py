import sys
import binascii
import math


class BmpFileHeader:
    def __init__(self):
        self.bfType = i_to_bytes(0, 2)
        self.bfSize = i_to_bytes(0, 4)
        self.bfReserved1 = i_to_bytes(0, 2)
        self.bfReserved2 = i_to_bytes(0, 2)
        self.bfOffBits = i_to_bytes(0, 4)


class BmpStructHeader:
    def __init__(self):
        self.biSize = i_to_bytes(0, 4)
        self.biWidth = i_to_bytes(0, 4)
        self.biHeight = i_to_bytes(0, 4)
        self.biPlanes = i_to_bytes(0, 2)
        self.biBitCount = i_to_bytes(0, 2)
        self.biCompression = i_to_bytes(0, 4)
        self.biSizeImage = i_to_bytes(0, 4)
        self.biXPelsPerMeter = i_to_bytes(0, 4)
        self.biYPelsPerMeter = i_to_bytes(0, 4)
        self.biClrUsed = i_to_bytes(0, 4)
        self.biClrImportant = i_to_bytes(0, 4)


class Bmp(BmpFileHeader, BmpStructHeader):
    def __init__(self):
        BmpFileHeader.__init__(self)
        BmpStructHeader.__init__(self)
        self.__bitSize = 0
        self.bits = []

    @property
    def width(self):
        return bytes_to_i(self.biWidth)

    @property
    def height(self):
        return bytes_to_i(self.biHeight)

    @property
    def bit_count(self):
        return bytes_to_i(self.biBitCount) // 8

    @property
    def width_step(self):
        return self.bit_count * self.width

    def parse(self, file_name):
        file = open(file_name, 'rb')
        # BmpFileHeader
        self.bfType = file.read(2)
        self.bfSize = file.read(4)
        self.bfReserved1 = file.read(2)
        self.bfReserved2 = file.read(2)
        self.bfOffBits = file.read(4)
        # BmpStructHeader
        self.biSize = file.read(4)
        self.biWidth = file.read(4)
        self.biHeight = file.read(4)
        self.biPlanes = file.read(2)
        self.biBitCount = file.read(2)
        # pixel size
        self.__bitSize = self.width * self.height * self.bit_count
        self.biCompression = file.read(4)
        self.biSizeImage = file.read(4)
        self.biXPelsPerMeter = file.read(4)
        self.biYPelsPerMeter = file.read(4)
        self.biClrUsed = file.read(4)
        self.biClrImportant = file.read(4)
        #  load pixel info
        count = 0
        while count < self.__bitSize:
            bit_count = 0
            while bit_count < (int.from_bytes(self.biBitCount, 'little') // 8):
                self.bits.append(file.read(1))
                bit_count += 1
            count += 1
        file.close()

    def generate(self, file_name):
        file = open(file_name, 'w')
        file.write('guaimage\n')
        file.write('1.0\n')
        file.write(str(self.width)+'\n')
        file.write(str(self.height)+'\n')

        i = 0
        temp = []
        while i < len(self.bits):
            end = i + int(self.width) * 3
            temp.append(self.bits[i: end])
            i += int(self.width) * 3

        for t in temp:
            c = 0
            arr = []
            while c < len(t):
                str2 = ''
                index = 0
                while index < 3:
                    str2 = str(bin(bytes_to_i(t[c+index])))[2:] + str2
                    index += 1
                arr.append(str2)
                c += 3
            str3 = ' '.join([str(int(x, 2)) for x in arr])
            file.write(str3+'\n')
        file.close()


def i_to_bytes(number, length, byteorder='little'):
    return number.to_bytes(length, byteorder)


def bytes_to_i(mbytes, byteorder='little'):
    return int.from_bytes(mbytes, byteorder)


def __main():
    bmp = Bmp()
    bmp.parse(sys.argv[1])
    # bmp.parse('bbmmpp.bmp')
    bmp.generate('bbmmpp.guaimage')

if __name__ == '__main__':
    __main()