#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "GuaList.h"

//
//创建两个单链表A、B,要求A、B 的元素按升序排列,输出单链表A、B,
//然后将A、B中相同的元素放在单链表C中，C也按升序排列，输出单链表C。


// interface
// 声明 结构名, 类型
struct GuaNode;
typedef struct GuaNode GuaList;
typedef int type;

// 结构的具体定义
struct GuaNode {
    type element;
    GuaList *next;
};


// 创建并返回一个 List
// element 是一个 int 数组
// numberOfElements 是数组的长度
// 在 C 语言中, 数组的长度信息要额外提供
GuaList *
GuaListCreate(int *element, int numberOfElements) {
    // assert 是用于确保一定条件的断言
    assert(numberOfElements >= 0);

    // malloc 申请一块内存, 并初始化一下
    GuaList *list = malloc(sizeof(GuaList));
    list->next = NULL;

    // 循环插入初始化元素
    for(int i = numberOfElements - 1; i >= 0; i--) {
        GuaList *n = malloc(sizeof(GuaList));
        n->element = element[i];
        n->next = list->next;
        list->next = n;
    }

    return list;
}

// 把一个 List 的数据打印出来
void
GuaListLog(GuaList *list) {
    GuaList *l = list->next;
    while(l != NULL) {
        printf("%d  ", l->element);
        l = l->next;
    }
    printf("\n");
}

int
GuaListLength(GuaList *list) {
    GuaList *l = list->next;
    type count = 0;
    while (l != NULL) {
        ++count;
        l = l->next;
    }
    return count;
}

bool
GuaListContains(GuaList *list, type element) {
    GuaList *l = list->next;
    while (l != NULL) {
        if (l->element == element) {
            return true;
        }
        l = l->next;
    }
    return false;
}

void
GuaListAppend(GuaList *list, type element) {
    GuaList *n = malloc(sizeof(GuaList));
    n->element = element;
    n->next = NULL;
    int len = GuaListLength(list);
    GuaList *l = list->next;
    for (int i = 0; i < len - 1; i++) {
        l = l->next;
    }
    l->next = n;
}

void
GuaListPrepend(GuaList *list, type element) {
    GuaList *n = malloc(sizeof(GuaList));
    n->element = element;
    n->next = list->next;
    list->next = n;
}

int
GuaListIndexOfElement(GuaList *list, type element) {
    int len = GuaListLength(list);
    GuaList *l = list->next;
    for (int i = 0; i < len; i++) {
        if (l->element == element) {
            return i;
        }
        l = l->next;
    }
    return -1;
}

void
GuaListInsertElementAtIndex(GuaList *list, type element, int index) {
    int len = GuaListLength(list);
    GuaList *l = list->next;
    for (int i = 1; i < len; i++) {
        if (i == index) {
            GuaListPrepend(l, element);
            return;
        }
        l = l->next;
    }
}
