#include <stdio.h>
#include <stdbool.h>

#include "GuaList.h"
#include "GuaTest.h"


void
testGuaListLength() {
    type a1[] = {1, 2, 3};
    int n1 = 3;
    GuaList *l1 = GuaListCreate(a1, n1);
    ensure(GuaListLength(l1) == n1, "test list length 1");

    type a2[] = {};
    int n2 = 0;
    GuaList *l2 = GuaListCreate(a2, n2);
    ensure(GuaListLength(l2) == n2, "test list length 2");
    
    type a3[] = {1, 2, 3, 4, 5};
    int n3 = 5;
    GuaList *l3 = GuaListCreate(a3, n3);
    ensure(GuaListLength(l3) == n3, "test list length 3");
}

void
testGuaListContains() {
    type a[] = {1, 2, 3, 4, 5};
    int n = 5;
    GuaList *l = GuaListCreate(a, n);
    int e1 = 1;
    ensure(GuaListContains(l, e1) == true, "test list contains 1");
    
    int e2 = 2;
    ensure(GuaListContains(l, e2) == true, "test list contains 2");
    
    int e3 = 0;
    ensure(GuaListContains(l, e3) == false, "test list contains 3");
}

void testGuaListAppend() {
    type a[] = {1, 2, 3};
    int n = 3;
    GuaList *l =  GuaListCreate(a, n);
    int e1 = 4;
    GuaListAppend(l, e1);
    ensure(GuaListLength(l) == 4, "test list append 1");
    
    int e2 = 5;
    GuaListAppend(l, e2);
    ensure(GuaListLength(l) == 5, "test list append 2");
    
    int e3 = 6;
    GuaListAppend(l, e3);
    ensure(GuaListLength(l) == 6, "test list append 3");
    
}


void testGuaListPrepend() {
    type a[] = {1, 2, 3};
    int n = 3;
    GuaList *l = GuaListCreate(a, n);
    
    int e1 = 1;
    GuaListPrepend(l, e1);
    ensure(GuaListLength(l) == 4, "test list prepend 1");
    
    int e2 = 2;
    GuaListPrepend(l, e2);
    ensure(GuaListLength(l) == 5, "test list prepend 2");
    
    
    int e3 = 3;
    GuaListPrepend(l, e3);
    ensure(GuaListLength(l) == 6, "test list prepend 3");
}

void
testGuaListIndexOfElement() {
    type a[] = {1, 2, 3};
    int n = 3;
    GuaList *l = GuaListCreate(a, n);
    
    int e1 = 1;
    ensure(GuaListIndexOfElement(l, e1) == 0, "text index of element 1");
    
    int e2 = 2;
    ensure(GuaListIndexOfElement(l, e2) == 1, "text index of element 2");
    
    int e3 = 5;
    ensure(GuaListIndexOfElement(l, e3) == -1, "text index of element 3");
}

void
testGuaListInsertElementAtIndex() {
    type a[] = {1, 2, 3};
    int n = 3;
    GuaList *l = GuaListCreate(a, n);
    int e1 = 5;
    int p1 = 1;
    GuaListInsertElementAtIndex(l, e1, p1);
    ensure(GuaListIndexOfElement(l, e1) == p1, "text list insert 1");
    
    int e2 = 6;
    int p2 = 2;
    GuaListInsertElementAtIndex(l, e2, p2);
    ensure(GuaListIndexOfElement(l, e2) == p2, "text list insert 2");
    
    int e3 = 7;
    int p3 = 2;
    GuaListInsertElementAtIndex(l, e3, p3);
    ensure(GuaListIndexOfElement(l, e3) == p3, "text list insert 3");
}


int
main(int argc, const char * argv[]) {
    testGuaListLength();
    testGuaListContains();
    testGuaListAppend();
    testGuaListPrepend();
    testGuaListIndexOfElement();
    testGuaListInsertElementAtIndex();

    // 返回 0 表示程序执行完毕并成功退出
    return 0;
}
