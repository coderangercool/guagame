class GuaCanvas extends GuaObject {
    constructor(selector) {
        super()
        let canvas = _e(selector)
        this.canvas = canvas
        this.context = canvas.getContext('2d')
        this.w = canvas.width
        this.h = canvas.height
        this.pixels = this.context.getImageData(0, 0, this.w, this.h)
        this.bytesPerPixel = 4
        this.lastX = 0
        this.lastY = 0
        this.lastSize = null
        this.startX = 0
        this.startY = 0
        this.isDrawing = false
        // this.lastPixels = this.pixels
        // this.pixelBuffer = this.pixels.data
    }


    render() {
        // 执行这个函数后, 才会实际地把图像画出来
        // ES6 新语法, 取出想要的属性并赋值给变量, 不懂自己搜「ES6 新语法」
        let {pixels, context} = this
        context.putImageData(pixels, 0, 0)
    }
    clear(color=GuaColor.white()) {
        // color GuaColor
        // 用 color 填充整个 canvas
        // 遍历每个像素点, 设置像素点的颜色
        let {w, h} = this
        for (let x = 0; x < w; x++) {
            for (let y = 0; y < h; y++) {
                this._setPixel(x, y, color)
            }
        }
        this.render()
    }

    _setPixel(x, y, color) {
        // color: GuaColor
        // 这个函数用来设置像素点, _ 开头表示这是一个内部函数, 这是我们的约定
        // 浮点转 int
        let int = Math.floor
        x = int(x)
        y = int(y)
        // 用座标算像素下标
        let i = (y * this.w + x) * this.bytesPerPixel
        // 设置像素
        let p = this.pixels.data
        let {r, g, b, a} = color
        // 一个像素 4 字节, 分别表示 r g b a
        p[i] = r
        p[i+1] = g
        p[i+2] = b
        p[i+3] = a
    }
    drawPoint(point, color=GuaColor.black()) {
        // point: GuaPoint
        let {w, h} = this
        let p = point
        if (p.x >= 0 && p.x <= w) {
            if (p.y >= 0 && p.y <= h) {
                this._setPixel(p.x, p.y, color)
            }
        }
    }
    drawLine(p1, p2, color=GuaColor.black()) {
        // p1 p2 分别是起点和终点, GuaPoint 类型
        // color GuaColor
        // 使用 drawPoint 函数来画线
        let [x1, y1, x2, y2] = [p1.x, p1.y, p2.x, p2.y]
        let dx = x2 - x1
        let dy = y2 - y1

        if(Math.abs(dx) > Math.abs(dy)) {
            let xmin = Math.min(x1, x2)
            let xmax = Math.max(x1, x2)
            let ratio = dx == 0 ? 0 : dy / dx
            for(let x = xmin; x < xmax; x++) {
                let y = y1 + (x - x1) * ratio
                this.drawPoint(GuaPoint.new(x, y), color)
            }
        } else {
            let ymin = Math.min(y1, y2)
            let ymax = Math.max(y1, y2)
            let ratio = dy == 0 ? 0 : dx / dy
            for(let y = ymin; y < ymax; y++) {
                let x = x1 + (y - y1) * ratio
                this.drawPoint(GuaPoint.new(x, y), color)
            }
        }
    }

    fill(point, size, color) {
        let {w, h} = size
        let {x, y} = point
        for (let xl = x; xl < x + w; xl++) {
            for (let yl = y; yl < y + h; yl++) {
                this._setPixel(xl, yl, color)
            }
        }
        this.render()
    }

    drawRect(upperLeft, size, fillColor=null, borderColor=GuaColor.black()) {
        // upperLeft: GuaPoint, 矩形左上角座标
        // size: GuaSize, 矩形尺寸
        // fillColor: GuaColor, 矩形的填充颜色, 默认为空, 表示不填充
        // borderColor: GuaColor, 矩形的的边框颜色, 默认伪黑色
        let {w, h} = size
        let {x : ox, y : oy} = upperLeft
        let ur = GuaPoint.new(ox + w, oy)
        let dl = GuaPoint.new(ox, oy + h)
        let dr = GuaPoint.new(ox + w, oy + h)
        this.drawLine(upperLeft, ur, borderColor)
        this.drawLine(ur, dr, borderColor)
        this.drawLine(dr, dl, borderColor)
        this.drawLine(dl, upperLeft, borderColor)
        let fillPoint = GuaPoint.new(ox + 1, oy + 1 )
        let fillSize =  GuaSize.new(w - 1, h - 1)
        this.fill(fillPoint, fillSize, fillColor)
    }
    __debug_draw_demo() {
        // 这是一个 demo 函数, 用来给你看看如何设置像素
        // ES6 新语法, 取出想要的属性并赋值给变量, 不懂自己搜「ES6 新语法」
        let {context, pixels} = this
        // 获取像素数据, data 是一个数组
        let data = pixels.data
        // 一个像素 4 字节, 分别表示 r g b a
        for (let i = 0; i < data.length; i += 4) {
            let [r, g, b, a] = data.slice(i, i + 4)
            r = 255
            a = 255
            data[i] = r
            data[i+3] = a
        }
        context.putImageData(pixels, 0, 0)
    }

    bindEvent(event, callback) {
        this.canvas.addEventListener(event, callback)
    }

    savePixels() {
        let lastData = this.context.createImageData(this.w, this.h)
        let dataArray = lastData.data
        for (let i = 0; i < dataArray.length; i += 4) {
            // let [r, g, b, a] = dataArray.slice(i, i + 4)
            // r = 255
            // a = 255
            dataArray[i] = this.pixels[i]
            dataArray[i+1] = this.pixels[i+1]
            dataArray[i+2] = this.pixels[i+2]
            dataArray[i+3] = this.pixels[i+3]
        }
        this.lastPixels = lastData
    }

    paintLine(e) {
        if (!this.isDrawing) {
            return
        }
        let lastPoint = GuaPoint.new(this.lastX, this.lastY)
        let startPoint = GuaPoint.new(this.startX, this.startY)
        this.drawLine(startPoint, lastPoint, GuaColor.new(0,0,0,0))
        let endPoint = GuaPoint.new(e.offsetX, e.offsetY);
        this.drawLine(startPoint, endPoint);
        this.render();
        [this.lastX, this.lastY] = [e.offsetX, e.offsetY];
    }

    paintRect(e) {
        if (!this.isDrawing) {
            return
        }
        let lastPoint = GuaPoint.new(this.lastX, this.lastY)
        let startPoint = GuaPoint.new(this.startX, this.startY)
        let size = GuaSize.new(Math.abs(e.offsetX - this.startX),  Math.abs(e.offsetY - this.startY))
        this.drawRect(lastPoint, size, GuaColor.new(0,0,0,0), GuaColor.new(0,0,0,0))
        this.drawRect(startPoint, size, GuaColor.new(0,0,0,0))
        this.render();
        [this.lastX, this.lastY] = [e.offsetX, e.offsetY];
        this.lastSize = size
    }
}
