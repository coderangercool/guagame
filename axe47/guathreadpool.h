#ifndef guathreadpool_h
#define guathreadpool_h

#include <stdio.h>
#include <pthread.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <time.h>

#endif /* guathreadpool_h */

struct _GuaThreadPool;
typedef struct _GuaThreadPool GuaThreadPool;

// 函数指针 typedef
// 这种东西就是照我的例子抄，别想背下来
typedef void *(GuaTaskCallback)(void *);


// 创建并返回一个 thread pool
// 参数为线程池的线程数量
GuaThreadPool* GuaThreadPoolNew(int numberOfThreads);

// 给线程池添加一个任务
// pool 是线程池
// callback 是线程要执行的函数
// arg 是传给
int GuaThreadPoolAddTask(GuaThreadPool *pool, GuaTaskCallback *callback, void *arg);

// 删除并释放一个线程池的资源
// pool 是要销毁的线程池
int GuaThreadPoolFree(GuaThreadPool *pool);

char *get_mime_type( char* name );
void *server(void);
int http_resonse(int client);
void send_error(FILE *wfd, int status, char *title, char *extra, char *text);
void send_file(FILE *wfd, char *path, struct stat *statbuf);
void exec_cgi(FILE *wfd, char *path, struct stat *statbuf);
void send_headers(FILE *wfd, int status, char *title, char *extra, char *mime, int length, time_t date);
