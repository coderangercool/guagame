#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

#include <SDL2/SDL.h>

#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"


static SDL_Window *window;
static SDL_Renderer *renderer;
static SDL_Rect srcrect;
static const char *r_title;
static int r_width;
static int r_height;

int
readConfig(lua_State *L) {
    if(luaL_dofile(L, "config.lua")) {
        printf("LUA ERROR: %s\n", lua_tostring(L, -1));
        return -1;
    }

    return 1;
}

int
config(lua_State *L) {
    r_title = lua_tostring(L, 1);
    r_width = lua_tonumber(L, 2);
    r_height = lua_tonumber(L, 3);
    
    return 1;
}

int
LuaDrawLine(lua_State *L) {
    int x1 = lua_tonumber(L, 1);
    int y1 = lua_tonumber(L, 2);
    int x2 = lua_tonumber(L, 3);
    int y2 = lua_tonumber(L, 4);

    SDL_RenderDrawLine(renderer, x1, y1, x2, y2);
    return 1;
}

int
LuaDrawPoint(lua_State *L) {
    int x = lua_tonumber(L, 1);
    int y = lua_tonumber(L, 2);

    SDL_RenderDrawPoint(renderer, x, y);
    return 1;
}

int
LuaDrawRect(lua_State *L) {
    srcrect.x = lua_tonumber(L, 1);
    srcrect.y = lua_tonumber(L, 2);
    srcrect.w = lua_tonumber(L, 3);
    srcrect.h = lua_tonumber(L, 4);

    SDL_RenderDrawRect(renderer, &srcrect);
    
    return 1;
}

int
LuaFillRect(lua_State *L) {
    srcrect.x = lua_tonumber(L, 1);
    srcrect.y = lua_tonumber(L, 2);
    srcrect.w = lua_tonumber(L, 3);
    srcrect.h = lua_tonumber(L, 4);

    SDL_RenderFillRect(renderer, &srcrect);
    
    return 1;
}

int
LuaSetColor(lua_State *L) {
    int r = lua_tonumber(L, 1);
    int g = lua_tonumber(L, 2);
    int b = lua_tonumber(L, 3);
    int a = lua_tonumber(L, 4);
    SDL_SetRenderDrawColor(renderer, r, g, b, a);

    return 1;
}


int
initsdl() {
    // 初始化 SDL
    SDL_Init(SDL_INIT_VIDEO);
    int width = r_width;
    int height = r_height;
    const char *title = r_title;
    // 创建窗口
    // 窗口标题 窗口x 窗口y 宽 高 额外参数
    window = SDL_CreateWindow(
        title,
        SDL_WINDOWPOS_UNDEFINED,
        SDL_WINDOWPOS_UNDEFINED,
        width,
        height,
        SDL_WINDOW_RESIZABLE
    );

    // 创建渲染层 文档如下
    // http://wiki.libsdl.org/SDL_CreateRenderer?highlight=%28SDL_CreateRenderer%29
    renderer = SDL_CreateRenderer(
        window,
        -1,
        SDL_RENDERER_ACCELERATED
    );

    return 0;
}

void
updateInput() {
    // 事件套路，参考我 github 的渲染器相关代码
    SDL_Event event;
    while(SDL_PollEvent(&event)) {
        switch(event.type) {
        case SDL_KEYDOWN:
            break;
        case SDL_QUIT:
            // 退出，点击关闭窗口按钮的事件
            SDL_DestroyRenderer(renderer);
            SDL_DestroyWindow(window);
            SDL_Quit();
            exit(0);
            break;
        }
    }
}

int
draw(lua_State *L) {
    // 设置背景颜色并清除屏幕
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderClear(renderer);

    // 设置画笔颜色
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);

    if(luaL_dofile(L, "draw.lua")) {
        printf("LUA ERROR: %s\n", lua_tostring(L, -1));
        return -1;
    }

    // 显示刚才画的内容
    SDL_RenderPresent(renderer);

    return 0;
}

int
main(int argc, char *argv[]) {
    lua_State *L = luaL_newstate();
    luaL_openlibs(L);

    lua_register(L, "config", config);
    lua_register(L, "drawLine", LuaDrawLine);
    lua_register(L, "drawPoint", LuaDrawPoint);
    lua_register(L, "drawRect", LuaDrawRect);
    lua_register(L, "fillRect", LuaFillRect);
    lua_register(L, "setColor", LuaSetColor);
    
    readConfig(L);

    initsdl();

    while(true) {
        updateInput();
        draw(L);
    }

    lua_close(L);

    return 0;
}