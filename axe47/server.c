#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"

#include "guathreadpool.h"



#define MAXLEN 4096 //buf最大长度
#define PORT 8787   //端口号
#define DOCUMENT "/Users/xiaojiong/www" //根目录
#define INDEX "index.html" //默认文件
#define PROTOCOL "HTTP/1.0" //默认协议
#define SERVER "axe" //服务器名称
#define RFC1123FMT "%a, %d %b %Y %H:%M:%S GMT" //RFC标准时间

//void *
//printTask(void *arg) {
//    int n = *(int *)arg;
//    //    free(arg);
//
//    printf("task %d \n", n);
//    sleep(1);
//
//    return NULL;
//}

int
main(int argc, const char *argv[]) {
    const int numberOfTasks = 20;
    
    GuaThreadPool *pool = GuaThreadPoolNew(5);
    
    for(int i = 0; i < numberOfTasks; ++i) {
        int *n = malloc(sizeof(int));
        *n = i;
//        GuaThreadPoolAddTask(pool, printTask, n);
        GuaThreadPoolAddTask(pool, server, n);
        printf("(server start %d)\n", i);
    }
    
    // 创建 lua 运行环境
    lua_State *L = luaL_newstate();
    // 加载 lua 标准库
    luaL_openlibs(L);
    // 载入 lua 文件并执行
    // 如果出错会打印出错原因
    if(luaL_dofile(L, "server.lua")) {
        printf("LUA ERROR: %s\n", lua_tostring(L, -1));
        return -1;
    }
    // 关闭 lua 运行环境
    lua_close(L);
    
    // GuaThreadPoolFree 要等所有的线程都退出后才会返回
    // 因为在队列中还没有执行的函数会执行后再退出
    GuaThreadPoolFree(pool);
    
    return 0;
}


void *
server() {
    struct sockaddr_in stSockAddr;
    int SocketFD = socket(PF_INET, SOCK_STREAM, 0);
    pid_t pid;
    
    if(-1 == SocketFD)
    {
        perror("can not create socket");
        exit(EXIT_FAILURE);
    }
    
    memset(&stSockAddr, 0, sizeof(struct sockaddr_in));
    
    stSockAddr.sin_family = AF_INET;
    stSockAddr.sin_port = htons(PORT);
    stSockAddr.sin_addr.s_addr = INADDR_ANY;
    
    if(-1 == bind(SocketFD,(const struct sockaddr *)&stSockAddr, sizeof(struct sockaddr_in)))
    {
        perror("error bind failed");
        close(SocketFD);
        exit(EXIT_FAILURE);
    }
    
    if(-1 == listen(SocketFD, 10))
    {
        perror("error listen failed");
        close(SocketFD);
        exit(EXIT_FAILURE);
    }
    while(1) {
        int client = accept(SocketFD, NULL, NULL);
        
        if(0 > client)
        {
            perror("error accept failed");
            close(SocketFD);
            exit(EXIT_FAILURE);
        }
        pid = fork();
        if (pid < 0) {
            perror("error fork failed");
            exit(EXIT_FAILURE);
        } else if(pid == 0) {
            http_resonse(client);
            close(client);
        } else {
            close(client);
            continue;
        }
    }
    return NULL;
}

int
http_resonse(int client) {
    char *method;
    char *path;
    char *protocol;
    char buf[MAXLEN];
    char file[MAXLEN] = DOCUMENT;
    struct stat statbuf;
    FILE *rfd, *wfd;
    rfd = fdopen(client, "a+");
    wfd = fdopen(dup(client), "w");
    
    //打印请求信息
    if (!fgets(buf, sizeof(buf), rfd))
        return -1;
    printf("URL: %s", buf);
    method = strtok(buf, " ");
    path = strtok(NULL, " ");
    protocol = strtok(NULL, "\r");
    if (!method || !path || !protocol)
        return -1;
    strcat(file, path);
    if (strcasecmp(method, "GET") != 0) {
        send_error(wfd, 501, "Not supported", NULL, "Method is not supported.");
    } else if (stat(file, &statbuf) < 0) {
        send_error(wfd, 404, "Not Found", NULL, "File not found.");
    }
    //输出文件
    if (strcasecmp(path, "/") == 0) {
        strcat(file, INDEX);
    }
    return 0;
}

void send_file(FILE *wfd, char *path, struct stat *statbuf) {
    char data[MAXLEN];
    int n;
    
    FILE *file = fopen(path, "r");
    if (!file) {
        send_error(wfd, 403, "Forbidden", NULL, "Access denied.");
    } else {
        int length = S_ISREG(statbuf->st_mode) ? statbuf->st_size : -1;
        send_headers(wfd, 200, "OK", NULL, get_mime_type(path), length, statbuf->st_mtime);
        while ((n = fread(data, 1, sizeof(data), file)) > 0)  {
            fwrite(data, 1, n, wfd);
        }
        fclose(file);
    }
}

void
send_error(FILE *wfd, int status, char *title, char *extra, char *text) {
    send_headers(wfd, status, title, extra, "text/html", -1, -1);
    fprintf(wfd, "<HTML><HEAD><TITLE>%d %s</TITLE></HEAD>\r\n", status, title);
    fprintf(wfd, "<BODY><H4>%d %s</H4>\r\n", status, title);
    fprintf(wfd, "%s\r\n", text);
    fprintf(wfd, "</BODY></HTML>\r\n");
}

void
send_headers(FILE *wfd, int status, char *title, char *extra, char *mime, int length, time_t date) {
    time_t now;
    char timebuf[MAXLEN];
    
    fprintf(wfd, "%s %d %s\r\n", PROTOCOL, status, title);
    fprintf(wfd, "Server: %s\r\n", SERVER);
    now = time(NULL);
    strftime(timebuf, sizeof(timebuf), RFC1123FMT, gmtime(&now));
    fprintf(wfd, "Date: %s\r\n", timebuf);
    if (extra) fprintf(wfd, "%s\r\n", extra);
    if (mime) fprintf(wfd, "Content-Type: %s\r\n", mime);
    if (length >= 0) fprintf(wfd, "Content-Length: %d\r\n", length);
    if (date != -1) {
        strftime(timebuf, sizeof(timebuf), RFC1123FMT, gmtime(&date));
        fprintf(wfd, "Last-Modified: %s\r\n", timebuf);
    }
    fprintf(wfd, "Connection: close\r\n");
    fprintf(wfd, "\r\n");
}

char *
get_mime_type( char* name )
{
    char* dot;
    dot = strrchr( name, '.' );
    if ( dot == (char*) 0 )
        return "text/plain; charset=UTF-8";
    if ( strcmp( dot, ".html" ) == 0 || strcmp( dot, ".htm" ) == 0 || strcmp( dot, ".sh" ) == 0)
        return "text/html; charset=UTF-8";
    return "text/plain; charset=UTF-8";
}
