
#ifndef view_h
#define view_h

#include <stdio.h>
#include <stdlib.h>
#include "/Library/Frameworks/SDL2.framework/Headers/SDL.h"

#endif /* view_h */

typedef struct _button GuaButton;

int initsdl(void);

void updateInput(void);

int draw(void);

int update(void);

int clear(void);

int GuaAddView(SDL_Renderer* renderer, GuaButton* button);

bool isOnButton(int x, int y);
