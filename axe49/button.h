#ifndef button_h
#define button_h

#include <stdio.h>
#include <stdlib.h>
#include "/Library/Frameworks/SDL2.framework/Headers/SDL.h"

#endif /* button_h */

typedef struct _button GuaButton;
struct _button {
    int x;
    int y;
    int w;
    int h;
    void* action;
};



GuaButton* GuaButtonNew(int posX, int posY, int width, int height);

int GuaButtonSetAction(GuaButton* button, void* actionClick);




