#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include "/Library/Frameworks/SDL2.framework/Headers/SDL.h"

#include "view.h"
#include "button.h"

static SDL_Window *window;
static SDL_Renderer *renderer;
static bool cool = false;

int
initsdl() {
    // 初始化 SDL
    SDL_Init(SDL_INIT_VIDEO);
    int width = 800;
    int height = 600;
    const char *title = "cool";
    // 创建窗口
    // 窗口标题 窗口x 窗口y 宽 高 额外参数
    window = SDL_CreateWindow(
                              title,
                              SDL_WINDOWPOS_UNDEFINED,
                              SDL_WINDOWPOS_UNDEFINED,
                              width,
                              height,
                              SDL_WINDOW_RESIZABLE
                              );
    
    // 创建渲染层 文档如下
    renderer = SDL_CreateRenderer(
                                  window,
                                  -1,
                                  SDL_RENDERER_ACCELERATED
                                  );
    
    return 0;
}

bool
isOnButton(int x, int y) {
    if (x > 100 && x <150) {
        if (y > 100 && y < 150) {
            return true;
        }
    }
    return false;
}

void
onMouseEvent(const SDL_Event *event) {
    if (event->button.button == SDL_BUTTON_LEFT) {
        if (isOnButton(event->button.x, event->button.y)) {
            cool = !cool;
        }
    }
}

void
updateInput() {
    // 事件套路，参考我 github 的渲染器相关代码
    SDL_Event event;
    while(SDL_PollEvent(&event)) {
        switch(event.type) {
            case SDL_MOUSEBUTTONDOWN:
                onMouseEvent(&event);

                break;
            case SDL_QUIT:
                // 退出，点击关闭窗口按钮的事件
                SDL_DestroyRenderer(renderer);
                SDL_DestroyWindow(window);
                SDL_Quit();
                exit(0);
                break;
        }
    }
}


int
update() {
    if (cool) {
        // 设置画笔颜色
        SDL_SetRenderDrawColor(renderer, 123, 123, 214, 255);
    } else {
        SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    }
    return 0;
}

int
clear() {
    // 设置背景颜色并清除屏幕
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderClear(renderer);
    return 0;
}

int
draw() {
    GuaButton* btn = GuaButtonNew(100, 100, 50, 50);
    GuaAddView(renderer, btn);
    
    // 显示刚才画的内容
    SDL_RenderPresent(renderer);
    
    free(btn);
    
    return 0;
}

int
GuaAddView(SDL_Renderer* renderer, GuaButton* button) {
    SDL_Rect srcrect;
    srcrect.x = button->x;
    srcrect.y = button->y;
    srcrect.w = button->w;
    srcrect.h = button->h;
    
    SDL_RenderFillRect(renderer, &srcrect);
    return 0;
}
