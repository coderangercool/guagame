#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include "/Library/Frameworks/SDL2.framework/Headers/SDL.h"

#include "view.h"
#include "button.h"

static SDL_Window *window;
static SDL_Renderer *renderer;

int
main(int argc, char *argv[]) {
    
    initsdl();
    
    while(true) {
        updateInput();
        clear();
        update();
        draw();
    }
    
    return 0;
}
