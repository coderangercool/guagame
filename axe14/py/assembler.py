from enum import Enum

class Type(Enum):
    order = 0       #指令
    register = 1    #寄存器
    memory = 2      #内存地址
    number = 3      #数字

class Token(object):
    def __init__(self, token_type, token_value):
        self.type = token_type
        self.value = token_value
    def __repr__(self):
        s = '({})'.format(self.value)
        return s

def machine_code(asm):
    length = len(asm)
    tokens = []
    spaces = '\n\t\r '
    register = 'xyz'
    digits = '0123456789'
    i = 0
    while i < length:
        c = asm[i]
        i += 1
        if c in spaces:
            continue
        elif c == '@':
            s = '@'
            while i < len(asm):
                if asm[i] in spaces:
                    break
                s += asm[i]
                i += 1
            t = Token(Type.memory, s)
            tokens.append(t)
        elif c in digits:
            end = 0
            for offset, char in enumerate(asm[i:]):
                if char not in digits:
                    end = offset
                    break
            n = asm[i-1:i+end]
            i += end
            t = Token(Type.number, int(n))
            tokens.append(t)
        elif c in register:
            t = Token(Type.register, c)
            tokens.append(t)
        elif ord(c) in range(65, 91) or ord(c) in range(97, 123):
            offset = i
            f = str(c)
            while offset < len(asm):
                t = asm[offset]
                if not (ord(t) in range(65, 91) or ord(t) in range(97, 123)):
                    i = offset + 1
                    t = Token(Type.order, f)
                    tokens.append(t)
                    break
                else:
                    f += asm[offset]
                    offset += 1
    result = [t.value for t in tokens]
    print(result)
    return result

def parse_asm(list):
    digits = "1234567890"
    for i, e in enumerate(list):
        if isinstance(e, str):
            if e[0] == '@':
                list[i] = int(e[1])
    d = {
        'x': 16,
        'y': 32,
        'z': 48,
        "set": 0,
        "load": 1,
        "add": 2,
        "save": 3,
    }
    for i, e in enumerate(list):
        if isinstance(e, str):
            list[i] = d[e]
    return ['{:08b}'.format(x) for x in list]

def main():
    temp = machine_code('''
        set x 1
        set y 2
        save x @0
        save y @1
        load @0 x
        load @1 y
        add x y z
        save z @2
    ''')
    print(parse_asm(temp))

if __name__ == '__main__':
    main()