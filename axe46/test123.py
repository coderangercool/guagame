import socket

HOST = ''
PORT = 8018

response_text = '''HTTP/1.x 200 OK 
Content-Type: text/html
<html> 
<head>
<title>test</title>
</head>

<p>cool</p>
</html>
'''

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST, PORT))


def serve():
    while True:
        s.listen(3)
        conn, addr = s.accept()
        request = conn.recv(1024).decode()
        method = request.split(' ')[0]
        src = request.split(' ')[1]

        if method == 'GET':
            if src == '/':
                content = response_text.encode()

            print('Connected by', addr)
            print('Request is:', request)
            conn.sendall(content)
        conn.close()


if __name__ == '__main__':
    serve()
