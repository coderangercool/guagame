#include <stdio.h>
#include <stdlib.h>
#include <Python/Python.h>

int main(int argc, const char * argv[]) {
    
    PyObject *pName, *pModule, *pDict, *pFunc, *pArgs, *pRetVal;
    
    Py_Initialize();
    
    PyRun_SimpleString("import sys");
    PyRun_SimpleString("sys.path.append('./')");
    
    if (!Py_IsInitialized()) {
        return -1;
    }
    
    pName = PyUnicode_FromString("test123");
    pModule = PyImport_Import(pName);
    
    if (!pModule) {
        PyErr_Print();
        printf("can't find test123.py");
        return -1;
    }
    
    pDict = PyModule_GetDict(pModule);
    
    if (!pDict) {
        return -1;
    }
    
    pFunc = PyDict_GetItemString(pDict, "serve");
    if (!pFunc || !PyCallable_Check(pFunc)) {
        PyErr_Print();
        printf("can't find function [serve]");
        return -1;
    }
    
    // 参数进栈
    pArgs = NULL;
    
    // PyObject* Py_BuildValue(char *format, ...)
    // 把C++的变量转换成一个Python对象。当需要从
    // C++传递变量到Python时，就会使用这个函数。此函数
    // 有点类似C的printf，但格式不同。常用的格式有
    // s 表示字符串，
    // i 表示整型变量，
    // f 表示浮点数，
    // O 表示一个Python对象。b=f(a) 0.1-0.01=0.09
//
//    PyTuple_SetItem(pArgs, 0, Py_BuildValue("i",3));
//    PyTuple_SetItem(pArgs, 1, Py_BuildValue("i",4));
    
    // 调用Python函数
    pRetVal = PyObject_CallObject(pFunc, pArgs);
//    printf("function return value : %d\r\n", _PyLong_AsInt(pRetVal));
    
    Py_DECREF(pName);
    Py_DECREF(pArgs);
    Py_DECREF(pModule);
    Py_DECREF(pRetVal);
    
    // 关闭Python
    Py_Finalize();
    
    return 0;
}
