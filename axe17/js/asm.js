const machineCode = function (asm) {
    let len = asm.length
    let tokens = []
    let spaces = '\t\n\r '
    let register = 'xyz'
    let digits = '1234567890'
    let letters = 'abcdefghijklmnopqrstuvwxyz'
    let i = 0
    while (i < len) {
        let c = asm[i]
        i += 1
        if (spaces.includes(c)) {
            continue;
        } else if (c === '@') {
            let s = '@'
            while (i < len) {
                if (spaces.includes(asm[i])) {
                    break
                }
                s += asm[i]
                i += 1
            }
            tokens.push(s)
        } else if (digits.includes(c)) {
            let end =  0
            let range = asm.slice(i)
            for (let j = 0; j < range.length; j++) {
                let e = range[j]
                if (!digits.includes(e)) {
                    end = j
                    break
                }
            }
            let n = asm.slice(i-1, i+end)
            i += end
            tokens.push(parseInt(n))
        } else if (register.includes(c)) {
            tokens.push(c)
        } else if (letters.includes(c)) {
            let offset = i
            let f = c
            while (offset < len) {
                if (!letters.includes(asm[offset])) {
                    i = offset + 1
                    tokens.push(f)
                    break
                } else {
                    f += asm[offset]
                    offset += 1
                }
            }
        }
    }
    console.log(tokens.toString())
    return tokens
}

function parseAsm(list) {
    for (let i = 0; i < list.length; i++) {
        let e = list[i];
        if (e[0] === '@') {
            list[i] = parseInt(e[1])
        }
    }
    let d = {
        'x': 256,
        'y': 512,
        'z': 768,
        "set": 0,
        "load": 1,
        "add": 2,
        "save": 3,
    }

    for (let i = 0; i < list.length; i++) {
        let e = list[i];
        if (typeof e === "string") {
            list[i] = d[e]
        }
    }
    console.log(list.toString())
    return list.map(e => e.toString(2))
}


function __main() {
    let temp = machineCode(`
        set x 1
        set y 2
        save x @0
        save y @1
        load @0 x
        load @1 y
        add x y z
        save z @2
    `)
    console.log(parseAsm(temp).toString())
}

__main()
