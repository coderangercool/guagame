log = console.log.bind(console)

ins = {
    "0": {
        name: "set",
        argNum: 2,
        regPos: 1,
    },
    "1": {
        name: "load",
        argNum: 2,
        regPos: 2,
    },
    "2": {
        name: "add",
        argNum: 3,
    },
    "3": {
        name: "save",
        argNum: 2,
        regPos: 1,
    },
    "7": {
        name: 'save_from_register',
        argNum: 2,
    }
}

func = {
    'add': {
        argNum: 3,
    },
    'save_from_register': {
        argNum: 2,
    }

}

register = {
    "16": 'x',
    "32": 'y',
    "48": 'z',
}

const run = function(memory) {
    /*
    这是一个虚拟机程序

    之前作业的 assembler.py 可以生成一个包含数字的数组
    参数 memory 就是那个数组

    run 函数将 memory 数组视为内存，可以执行这个内存
    你需要用变量来模拟寄存器，模拟程序的执行

    稍后会给出一个用于测试的 memory 例子并更新在 #general
    你现在可以用自己生成的内容来测试

    注意，memory 目前只能支持 256 字节
    因为 pc 寄存器只有 8 位（1 字节）
    */

    let mm = Array(65536).fill(0)
    const instruction = memory.map(e => e.toString())
    log(instruction.toString())
    let i  = 0;
    while (i < instruction.length) {
        let t = instruction[i]
        if (t in ins) {
            instruction[i] = ins[t].name
            let rIndex = i + ins[t].regPos
            let rValue = instruction[rIndex]
            instruction[rIndex] = register[rValue]
            i = i + ins[t].argNum + 1;
        }
    }
    for (let j = 0; j < instruction.length; j++) {
        let e = instruction[j]
        if (e in func) {
            let argNum = func[e].argNum
            for (let k = 1; k <= argNum; k++) {
                let rIndex = j + k
                let rValue = instruction[rIndex]
                instruction[rIndex] = register[rValue]
            }
        }
    }
    log(instruction.toString())
    let vars = {}
    for (let j = 0; j < instruction.length; j++) {
        let ins = instruction
        let e = instruction[j]
        if (e === 'set') {
            vars[ins[j+1]] = ins[j+2]
        }
        if (e === 'save_from_register') {
            let y = parseInt(vars['y'])
            let x = parseInt(vars['x'])
            mm[y] = x
            log('...mm', mm[y])
        }
        if (e === 'add') {
            let a = parseInt(vars[ins[j+1]])
            let b = parseInt(vars[ins[j+2]])
            vars[ins[j+3]] = a + b
        }
    }
    log(mm.slice(65436).toString())
    log(vars)
    draw(mm , vars)
}

function draw(data, vars) {
    let pixelData = data.slice(155)
    let color = {r:255, g:0, b:0, a:255}
    log('red', color)
    let cvs = document.querySelector('#id-canvas')
    let ctx = cvs.getContext('2d')
    let cvsPixels = ctx.getImageData(0, 0, cvs.width, cvs.height)
    let pixelsArray = cvsPixels.data
    let {r, g, b, a} = color
    for (let j = 0; j < pixelData.length; j++) {
        if (pixelData[j] !== 0) {
            let index = j * 4
            pixelsArray[index] = r
            pixelsArray[index+1] = g
            pixelsArray[index+2] = b
            pixelsArray[index+3] = a
        }
    }
    ctx.putImageData(cvsPixels, 0, 0)
}

function __main() {
    // const memory = ['00000000', '00010000', '00000001', '00000000', '00100000', '00000010', '00000011', '00010000', '00000000', '00000011', '00100000', '00000001', '00000001', '00000000', '00010000', '00000001', '00000001', '00100000', '00000010', '00010000', '00100000', '00110000', '00000011', '00110000', '00000010']
    const memory = [
        0b00000000, // set
        0b00100000, // y
        0b10011011, // 155, 左上角第一个像素
        0b00000000, // set
        0b00110000, // z
        0b00010110, // 22, 用于斜方向设置像素，每两排设置一个
        0b00000000, // set
        0b00010000, // x
        0b11000011, // 红色
        0b00000111, // save_from_register
        0b00010000, // x
        0b00100000, // y
        // 设置新像素点
        0b00000010, // add
        0b00100000, // y
        0b00110000, // z
        0b00100000, // y
        0b00000111, // save_from_register
        0b00010000, // x
        0b00100000, // y
        // 设置新像素点
        0b00000010, // add
        0b00100000, // y
        0b00110000, // z
        0b00100000, // y
        0b00000111, // save_from_register
        0b00010000, // x
        0b00100000, // y
        // 设置新像素点
        0b00000010, // add
        0b00100000, // y
        0b00110000, // z
        0b00100000, // y
        0b00000111, // save_from_register
        0b00010000, // x
        0b00100000, // y
        // 设置新像素点
        0b00000010, // add
        0b00100000, // y
        0b00110000, // z
        0b00100000, // y
        0b00000111, // save_from_register
        0b00010000, // x
        0b00100000, // y
        // 结果是在显示屏上显示一条斜线，一共 5 个红色的像素点
    ]
    log(run(memory))
}

__main();