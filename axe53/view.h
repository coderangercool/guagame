#ifndef view_h
#define view_h

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include <SDL2/SDL.h>
#include <SDL2_ttf/SDL_ttf.h>

#include "button.h"
#include "label.h"
#include "input.h"
#include "switch.h"
#include "slider.h"


#endif /* view_h */

struct _view;
typedef struct _view GuaView;

int GuaButtonAddView(SDL_Renderer* renderer, GuaButton* button);

SDL_Rect *GuaInputAddView(SDL_Renderer* renderer, GuaInput *input);

SDL_Rect *GuaLabelAddView(SDL_Renderer* renderer, GuaLabel *label);

int GuaSwitchAddView(SDL_Renderer *renderer, GuaSwitch *swt);

int GuaSliderAddView(SDL_Renderer *renderer, GuaSlider *sld);

GuaView* initsdl();

void closeSDL(SDL_Window* window ,SDL_Renderer* renderer);

void updateInput(SDL_Window* window, SDL_Renderer* renderer, char *inputText, GuaSwitch *swt, GuaSlider *sld);

int draw(lua_State *L, SDL_Renderer* renderer);

SDL_Texture *GuaTextRenderTexture(SDL_Renderer *renderer, TTF_Font *font, const char *text, SDL_Color color);

void GuaTextSetPosition(SDL_Texture *texture, int x, int y, SDL_Rect *rect);


