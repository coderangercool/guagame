#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include <SDL2/SDL.h>
#include <SDL2_ttf/SDL_ttf.h>

#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"

#include "view.h"
#include "button.h"
#include "label.h"
#include "label.h"
#include "switch.h"
#include "slider.h"


struct _view {
    SDL_Window *window;
    SDL_Renderer *renderer;
};

struct _input {
    int x;
    int y;
    int w;
    int h;
    char *inputText;
};

struct _label {
    int x;
    int y;
    int w;
    int h;
    char *labelText;
};

struct _switch {
    GuaButton *back;
    GuaButton *front;
    int status;
    void *callback;
};

struct _slider {
    GuaButton *back;
    GuaButton *front;
    int status;
};

GuaView *view;
static SDL_Rect srcrect;

int
LuaDrawLine(lua_State *L) {
    int x1 = lua_tonumber(L, 1);
    int y1 = lua_tonumber(L, 2);
    int x2 = lua_tonumber(L, 3);
    int y2 = lua_tonumber(L, 4);
    
    SDL_RenderDrawLine(view->renderer, x1, y1, x2, y2);
    return 1;
}

int
LuaFillRect(lua_State *L) {
    srcrect.x = lua_tonumber(L, 1);
    srcrect.y = lua_tonumber(L, 2);
    srcrect.w = lua_tonumber(L, 3);
    srcrect.h = lua_tonumber(L, 4);
    
    SDL_SetRenderDrawColor(view->renderer, 255, 255, 255, 255);
    SDL_RenderFillRect(view->renderer, &srcrect);
    
    return 1;
}

int
LuaDrawLabel(lua_State *L) {
    int x = lua_tonumber(L, 1);
    int y = lua_tonumber(L, 2);
    int w = lua_tonumber(L, 3);
    int h = lua_tonumber(L, 4);
    const char *text = lua_tostring(L, 5);
    GuaLabel *label = GuaLabelNew(x, y, w, h, text);
    
    SDL_Rect *rectLabel = GuaLabelAddView(view->renderer, label);
    
    return 1;
}

int
main(int argc, char *argv[]) {
    
    char inputText[100] = "";
    
    lua_State *L = luaL_newstate();
    luaL_openlibs(L);
    
    lua_register(L, "drawLine", LuaDrawLine);
    lua_register(L, "fillRect", LuaFillRect);
    lua_register(L, "drawLabel", LuaDrawLabel);
    
    view = initsdl();
    
    
    // 初始化字体
    TTF_Init();
    const char *fontPath = "OpenSans-Regular.ttf";
    // 打开字体 参数是 fontpath and fontsize
    TTF_Font *font = TTF_OpenFont(fontPath, 34);
    
    // 生成字体图片并设置图片座标
    SDL_Color color = {122, 123, 166, 155,};
    SDL_StartTextInput();
    
    //画Input
    GuaInput *input = GuaInputNew(100, 100, 300, 50, inputText);
    //Label
    GuaLabel *label = GuaLabelNew(100, 200, 100, 50, "cool");
    //switch
    GuaSwitch *swt = GuaNewSwitch(100, 300, 200, 100, 50);
    //slider
    GuaSlider *sld = GuaNewSlider(100, 500, 300, 20, 50);
    
    while(true) {
        // 更新输入
        updateInput(view->window, view->renderer, input->inputText, swt, sld);
        
        // 画图
        draw(L, view->renderer);
//
//        SDL_Rect *rect = GuaInputAddView(view->renderer, input);
//
//        SDL_Rect *rectLabel = GuaLabelAddView(view->renderer, label);
//
//        GuaSwitchAddView(view->renderer, swt);
//
//        GuaSliderAddView(view->renderer, sld);

        
//        // 画文字 注意参数
//        SDL_Texture *textTexture = GuaTextRenderTexture(view->renderer, font, inputText, color);
//        GuaTextSetPosition(textTexture, rect->x, rect->y, rect);
//        SDL_RenderCopy(view->renderer, textTexture, NULL, rect);
//
        //画label文字
//        SDL_Texture *textTextureLabel = GuaTextRenderTexture(view->renderer, font, label->labelText, color);
//        GuaTextSetPosition(textTextureLabel, rectLabel->x, rectLabel->y, rectLabel);
//        SDL_RenderCopy(view->renderer, textTextureLabel, NULL, rectLabel);

//        // 显示
        SDL_RenderPresent(view->renderer);
//        free(rect);
//        free(rectLabel);
//        SDL_DestroyTexture(textTexture);
//        SDL_DestroyTexture(textTextureLabel);

    }
    
    lua_close(L);
    
    // 释放字体资源, 这是演示, 实际上代码执行不到这里, 前面是个 while true
    TTF_Quit();
    
    return 0;
}
