#ifndef input_h
#define input_h

#include <stdio.h>
#include <stdlib.h>
#include <SDL2/SDL.h>

#endif /* input_h */

struct _input;
typedef struct _input GuaInput;

GuaInput *GuaInputNew(int x, int y, int w, int h, char *inputText);
