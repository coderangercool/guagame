#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <SDL2/SDL.h>

#include "switch.h"
#include "button.h"

struct _switch {
    GuaButton *back;
    GuaButton *front;
    int status;
};

struct _button {
    int x;
    int y;
    int w;
    int h;
    void* action;
};

GuaSwitch *GuaNewSwitch(int x, int y, int wback, int wfront, int h) {
    GuaSwitch *switchbtn = malloc(sizeof(GuaSwitch));
    GuaButton *back = malloc(sizeof(GuaButton));
    GuaButton *front = malloc(sizeof(GuaButton));
    back->x = x;
    back->y = y;
    back->w = wback;
    back->h = h;
    front->x = x;
    front->y = y;
    front->w = wfront;
    front->h = h;
    switchbtn->back = back;
    switchbtn->front = front;
    switchbtn->status = 0;

    return switchbtn;
}

int
GuaSwitchClick(GuaSwitch *swt) {
    if (swt->status == 0) {
        swt->status = 1;
    } else {
        swt->status = 0;
    }
    if (swt->status == 1) {
        swt->front->x = swt->front->x + swt->front->w;
    } else {
        swt->front->x = swt->back->x;
    }
    return 0;
}
