#ifndef switch_h
#define switch_h

#include <stdio.h>
#include <stdlib.h>
#include <SDL2/SDL.h>

#endif /* switch_h */

#include "button.h"

struct _switch;
typedef struct _switch GuaSwitch;

GuaSwitch *GuaNewSwitch(int x, int y, int wback, int wfront, int h);

int GuaSwitchClick(GuaSwitch *swt);
