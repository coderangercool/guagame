#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

#include <SDL2/SDL.h>
#include <SDL2_ttf/SDL_ttf.h>

#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"

#include "button.h"
#include "label.h"
#include "view.h"
#include "switch.h"
#include "slider.h"


struct _view {
    SDL_Window *window;
    SDL_Renderer *renderer;
};

struct _button {
    int x;
    int y;
    int w;
    int h;
    void* action;
};


struct _label {
    int x;
    int y;
    int w;
    int h;
    char *labelText;
};

struct _input {
    int x;
    int y;
    int w;
    int h;
    char *inputText;
};

struct _switch {
    GuaButton *back;
    GuaButton *front;
    int status;
};

struct _slider {
    GuaButton *back;
    GuaButton *front;
    int status;
};

GuaView *
initsdl() {
    GuaView *view = malloc(sizeof(GuaView));
    // 初始化 SDL
    SDL_Init(SDL_INIT_VIDEO);
    int width = 1200;
    int height = 800;
    // 创建窗口
    // 窗口标题 窗口x 窗口y 宽 高 额外参数
    view->window = SDL_CreateWindow(
                              "cool",
                              SDL_WINDOWPOS_UNDEFINED,
                              SDL_WINDOWPOS_UNDEFINED,+
                              width,
                              height,
                              SDL_WINDOW_RESIZABLE
                              );
    
    // 创建渲染层 文档如下
    // http://wiki.libsdl.org/SDL_CreateRenderer?highlight=%28SDL_CreateRenderer%29
    view->renderer = SDL_CreateRenderer(
                                  view->window,
                                  -1,
                                  SDL_RENDERER_ACCELERATED
                                  );
    
    return view;
}

void
closeSDL(SDL_Window* window ,SDL_Renderer* renderer) {
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
}

bool
isOnButton(int x, int y, int swtX, int swtXX, int swtY, int swtYY) {
    if (x > swtX && x < swtXX) {
        if (y > swtY && y < swtYY) {
            return true;
        }
    }
    return false;
}

void
updateInput(SDL_Window *window, SDL_Renderer* renderer, char* inputText, GuaSwitch *swt, GuaSlider *sld) {
    // 事件套路，参考我 github 的渲染器相关代码
    SDL_Event event;
    while(SDL_PollEvent(&event)) {
        switch(event.type) {
            case SDL_KEYDOWN:
                break;
            case SDL_MOUSEBUTTONDOWN:
                if (event.button.button == SDL_BUTTON_LEFT) {
                    int xx = swt->back->x + swt->back->w;
                    int yy = swt->back->y + swt->back->h;
                    
                    int sx = sld->back->x + sld->back->w;
                    int sy = sld->back->y + sld->back->h;
                    
                    if (isOnButton(event.button.x, event.button.y, swt->back->x, xx, swt->back->y, yy)) {
                        GuaSwitchClick(swt);
                    }
                    
                    if (isOnButton(event.button.x, event.button.y, sld->back->x, sx, sld->back->y, sy)) {
                        GuaSliderDrag(sld, event.button.x);
                    }
                }
                break;
            case SDL_MOUSEMOTION:
                if (event.button.button == SDL_BUTTON_LEFT) {
                    int sx = sld->back->x + sld->back->w;
                    int sy = sld->back->y + sld->back->h;
                    if (isOnButton(event.button.x, event.button.y, sld->back->x, sx, sld->back->y, sy)) {
                        GuaSliderDrag(sld, event.button.x);
                    }
                }
                break;
            case SDL_QUIT:
                // 退出，点击关闭窗口按钮的事件
                closeSDL(window, renderer);
                exit(0);
                break;
            case SDL_TEXTINPUT:
                strcat(inputText, event.text.text);
                break;
        }
    }
}

SDL_Rect *
GuaInputAddView(SDL_Renderer* renderer, GuaInput *input) {
    SDL_Rect *srcrect = malloc(sizeof(SDL_Rect));
    srcrect->x = input->x;
    srcrect->y = input->y;
    srcrect->w = input->w;
    srcrect->h = input->h;
    
    SDL_RenderFillRect(renderer, srcrect);
    return srcrect;
}

SDL_Rect *
GuaLabelAddView(SDL_Renderer* renderer, GuaLabel *label) {
    SDL_SetRenderDrawColor(renderer, 0, 123, 215, 255);
    SDL_Rect *srcrect = malloc(sizeof(SDL_Rect));
    srcrect->x = label->x;
    srcrect->y = label->y;
    srcrect->w = label->w;
    srcrect->h = label->h;
    
    SDL_RenderFillRect(renderer, srcrect);
    return srcrect;
}

int
GuaSwitchAddView(SDL_Renderer *renderer, GuaSwitch *swt) {
    SDL_Rect *srcrect1 = malloc(sizeof(SDL_Rect));
    srcrect1->x = swt->back->x;
    srcrect1->y = swt->back->y;
    srcrect1->w = swt->back->w;
    srcrect1->h = swt->back->h;
    
    SDL_Rect *srcrect2 = malloc(sizeof(SDL_Rect));
    srcrect2->x = swt->front->x;
    srcrect2->y = swt->front->y;
    srcrect2->w = swt->front->w;
    srcrect2->h = swt->front->h;
    
    SDL_RenderFillRect(renderer, srcrect1);
    
    SDL_SetRenderDrawColor(renderer, 0, 212, 215, 255);

    SDL_RenderFillRect(renderer, srcrect2);

    return 1;
}

int
GuaSliderAddView(SDL_Renderer *renderer, GuaSlider *sld) {
    SDL_Rect *srcrect1 = malloc(sizeof(SDL_Rect));
    srcrect1->x = sld->back->x;
    srcrect1->y = sld->back->y;
    srcrect1->w = sld->back->w;
    srcrect1->h = sld->back->h;
    
    SDL_Rect *srcrect2 = malloc(sizeof(SDL_Rect));
    srcrect2->x = sld->front->x;
    srcrect2->y = sld->front->y;
    srcrect2->w = sld->front->w;
    srcrect2->h = sld->front->h;
    
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    
    SDL_RenderFillRect(renderer, srcrect1);
    
    SDL_SetRenderDrawColor(renderer, 0, 212, 215, 255);
    
    SDL_RenderFillRect(renderer, srcrect2);
    
    return 1;
}

int
draw(lua_State *L, SDL_Renderer* renderer) {
    // 设置背景颜色并清除屏幕
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderClear(renderer);
    
    // 设置画笔颜色
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    
    if(luaL_dofile(L, "draw.lua")) {
        printf("LUA ERROR: %s\n", lua_tostring(L, -1));
        return -1;
    }
    
    return 0;
}

SDL_Texture *
GuaTextRenderTexture(SDL_Renderer *renderer, TTF_Font *font, const char *text, SDL_Color color) {
    // 用 TTF_RenderUNICODE_Solid 可以生成汉字字体
    // 不过我们用的字体只有英文字体
    SDL_Surface *surface = TTF_RenderText_Solid(font, text, color);
    SDL_Texture *texture = SDL_CreateTextureFromSurface(renderer, surface);
    SDL_FreeSurface(surface);
    
    return texture;
}

void
GuaTextSetPosition(SDL_Texture *texture, int x, int y, SDL_Rect *rect) {
    SDL_QueryTexture(texture, NULL, NULL, &rect->w, &rect->h);
    rect->x = x;
    rect->y = y;
    // printf("GuaTextSetPosition: %d %d\n", rect->w, rect->h);
}
