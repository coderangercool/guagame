gutter = 5
width = 50
startX1 = 100
startX2 = 155
startY = 100

function drawLineLabel(x, y, num, text)
    for i = 1, num do
        yy = y + (width + gutter) * (i - 1)
        drawLabel(x, yy, width, width, text)
    end
end

drawLineLabel(startX1, startY, 3, "cool")

drawLineLabel(startX2, startY, 3, "cool")

fillRect(210, 100, 500, 500)
