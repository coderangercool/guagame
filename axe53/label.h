#ifndef label_h
#define label_h

#include <stdio.h>
#include <stdlib.h>
#include <SDL2/SDL.h>

#endif /* label_h */

struct _label;
typedef struct _label GuaLabel;

GuaLabel *GuaLabelNew(int x, int y, int w, int h, char *labelText);

