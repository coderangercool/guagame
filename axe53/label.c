#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <SDL2/SDL.h>

#include "label.h"

struct _label {
    int x;
    int y;
    int w;
    int h;
    char *labelText;
};

GuaLabel *
GuaLabelNew(int x, int y, int w, int h, char *text) {
    GuaLabel *label = malloc(sizeof(GuaLabel));
    label->x = x;
    label->y = y;
    label->w = w;
    label->h = h;
    label->labelText = text;
    
    return label;
}



