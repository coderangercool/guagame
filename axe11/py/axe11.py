from operator import add, sub, mul, truediv, mod
from enum import Enum

class Type(Enum):
    auto = 0            # auto 就是 6 个单字符符号, 用来方便写代码的
    colon = 1           # :
    comma = 2           # ,
    braceLeft = 3       # {
    braceRight = 4      # }
    bracketLeft = 5     # [
    bracketRight = 6    # ]
    number = 7          # 169
    string = 8          # "name"
    btrue = 9           # True
    bfalse = 10         # False
    bnull = 11          # Null
    functions = 12      # function
    operatorBool = 13   # <  > ! =
    operatorMath = 14   # + - * / %

class Token(object):
    def __init__(self, token_type, token_value):
        super(Token, self).__init__()
        # 用表驱动法处理 if
        d = {
            ':': Type.colon,
            ',': Type.comma,
            '{': Type.braceLeft,
            '}': Type.braceRight,
            '[': Type.bracketLeft,
            ']': Type.bracketRight,
        }
        if token_type == Type.auto:
            self.type = d[token_value]
        else:
            self.type = token_type
        self.value = token_value

    def __repr__(self):
        return '({})'.format(self.value)


def string_end(code, index):
    """
    code = "abc"
    index = 1
    """
    s = ''
    offset = index
    trans = {
        '\"': '\"',
        '\t': '\t',
        '\n': '\n',
        '\\': '\\',
    }
    while offset < len(code):
        c = code[offset]
        if c == '"':
            # 找到了字符串的结尾
            # s = code[index:offset]
            return (s, offset)
        elif c == '\\':
            # 处理转义符, 现在只支持 \"
            # if code[offset+1] == '"':
            #     s += '"'
            #     offset += 2
            (s, offset) = transformToken(s, code, offset , trans)
            # else:
            #     # 这是一个错误, 非法转义符
            #     pass
        else:
            s += c
            offset += 1
    # 程序出错, 没有找到反引号 "
    pass

def transformToken(s, code, offset, trans):
    key = code[offset+1]
    if key in trans:
        s += trans[key]
        offset += 2
        return (s, offset)
    else:
        raise Exception('not supported token')

def s_tokens(code):
    length = len(code)
    tokens = []
    spaces = '\n\t\r '
    digits = '1234567890'
    opBool = '<>!='
    opMath = '+-*/%'
    # 当前下标
    i = 0
    while i < length:
        # 先看看当前应该处理啥
        c = code[i]
        i += 1
        if c in spaces:
            # 空白符号要跳过, space tab return
            continue
        elif c in ':,{}[]':
            # 处理 6 种单个符号
            t = Token(Type.auto, c)
            tokens.append(t)
        elif c in opBool:
            t = Token(Type.operatorBool, c)
            tokens.append(t)
        elif c in opMath:
            t = Token(Type.operatorMath, c)
            tokens.append(t)
        elif c == '"':
            # 处理字符串
            s, offset = string_end(code, i)
            i = offset + 1
            # print('i, offset', i, offset, s, code[offset])
            t = Token(Type.string, s)
            tokens.append(t)
        elif c in digits:
            # 处理数字, 现在不支持小数和负数
            end = 0
            for offset, char in enumerate(code[i:]):
                if char not in digits:
                    end = offset
                    break
            n = code[i-1:i+end]
            i += end
            t = Token(Type.number, int(n))
            tokens.append(t)
        elif ord(c) in range(65, 91) or ord(c) in range(97, 123):
            offset = i
            f = str(c)
            while offset < len(code):
                if code[offset] in spaces:
                    i = offset + 1
                    t = Token(Type.functions, f)
                    tokens.append(t)
                    break
                else:
                    f += code[offset]
                    offset += 1
        elif (c + code[i] + code[i+1] == 'yes') and code[i+2] in spaces:
            t = Token(Type.btrue, True)
            i += 3
            tokens.append(t)
        elif (c + code[i] == 'no') and code[i+1] in spaces:
            i += 3
            t = Token(Type.bfalse, False)
            tokens.append(t)
        # elif c == 'n':
        #     i += 3
        #     t = Token(Type.bnull, None)
        #     tokens.append(t)
        else:
            # 出错了
            pass
    result = [t.value for t in tokens]
    return result

def reduce(fn, l):
    reduced = l[0]
    rest = l[1:]
    for x in rest:
        reduced = fn(reduced, x)
    return reduced

def applyMath(tokens):
    first = tokens[0]
    rest = tokens[1:]
    fnMath = {
        "+": add,
        "-": sub,
        "*": mul,
        "/": truediv,
        '%': mod
    }
    return reduce(fnMath[first], rest)


# def parseList(tokens):
#     tLen = len(tokens)
#     tokens = tokens[1:(tLen-1)]
#     res = []
#     for i, t in enumerate(tokens):
#         res.append(t)
#         if t == '[':
#             temp = []





def apply(code):
    """
    code 是一个字符串

    我们定义一个语言, 使用 S 表达式作为语法
    S 表达式简单说来就是操作符在前
    具体代码说明和例子如下
    0, 每个表达式都是一个值, 操作符(可以认为是函数)和操作数(可以认为是参数)用空格隔开
    1, ; 到行末是单行注释, 没有多行注释
    2, 支持 + - * / % 五种基本数学操作和 = ! > < 四种逻辑操作(相等 不等 大于 小于)
        逻辑操作的结果是布尔值 yes 和 no(这 2 个是关键字)
    3, 支持表达式嵌套
    4, 支持内置函数 log, 作用是输出参数字符串
    5, 支持条件表达式 if

    [+ 1 2]         ; 表达式的值是 3
    [* 2 3 4]       ; 表达式的值是 24
    [log "hello"]   ; 输出 hello, 表达式的值是 null(关键字 表示空)
    [+ 1 [- 2 3]]   ; 表达式的值是 0, 相当于普通语法的 1 + (2 - 3)
    [if [> 2 1] 3 4]; 表达式的值是 3
    [if yes
        [log "成功"]
        [log "没成功"]
    ]
    """
    opBool = '<>!='
    opMath = '+-*/%'
    fns = ['log',]
    tokens = s_tokens(code)
    tLen = len(tokens)
    tokens = tokens[1:tLen-1]
    op, rest = tokens[0], tokens[1:]
    if op in opMath:
        res = applyMath(tokens)
        return res
    if op in fns:
        if op == 'log':
            print(rest)
            return


print(apply('[log 2 3 4]'))


# print(s_tokens("[if [> 2 1] 3 4]"))
