#ifndef guaevent_h
#define guaevent_h

struct _GuaEvent;
typedef struct _GuaEvent GuaEvent;
struct _GuaEvent {
    int type;
    
    int state;
    
    int key;
    
    int x;
    int y;
};

#endif /* guaevent_h */
