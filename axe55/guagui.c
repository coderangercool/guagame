#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>

#include <SDL2/SDL.h>
#include <SDL2_ttf/SDL_ttf.h>

#include "lua.h"
#include "lualib.h"
#include "lauxlib.h"

#include "guagui.h"

static SDL_Window *window;
static SDL_Renderer *renderer;
static lua_State *L;
static TTF_Font *font;

static GuaView *rootView = NULL;

GuaView *
GuaGuiInit(void) {
    SDL_Init(SDL_INIT_VIDEO);
    int width = 800;
    int height = 600;
    
    window = SDL_CreateWindow("cool", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width, height, SDL_WINDOW_RESIZABLE);
    
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
    
    L = luaL_newstate();
    luaL_openlibs(L);
    
    const char *fontPath = "OpenSans-Regular.ttf";
    
    font = TTF_OpenFont(fontPath, 34);
    
    GuaRect frame = {
        0, 0, width, height
    };
    
    rootView = GuaViewCreate(frame);
    rootView->renderer = renderer;
    
    return rootView;
}

void
GuaGuiClose(void) {
    lua_close(L);
    TTF_Quit();
    
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
}


static void
_onKey(SDL_Event event) {
    printf("on key\n");
}

static void
_onMouse(SDL_Event event) {
    GuaEvent e;
    e.type = 1;
    
    if (event.type == SDL_MOUSEBUTTONDOWN) {
        e.state = 1;
    } else if (event.type == SDL_MOUSEBUTTONUP) {
        e.state = 2;
    } else if (event.type == SDL_MOUSEMOTION){
        e.state = 3;
    }
    
    e.x = event.motion.x;
    e.y = event.motion.y;
    GuaViewOnEvent(rootView, e);
}

static void
_updataInput(void) {
    SDL_Event event;
    while (SDL_PollEvent(&event)) {
        switch (event.type) {
            case SDL_KEYDOWN:
            case SDL_KEYUP:
                _onKey(event);
                break;
            case SDL_MOUSEBUTTONDOWN:
            case SDL_MOUSEBUTTONUP:
            case SDL_MOUSEMOTION:
                _onMouse(event);
                break;
            case SDL_QUIT:
                GuaGuiClose();
                exit(0);
                break;
        }
    }
}

static int
_draw(void) {
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
    SDL_RenderClear(renderer);
    
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    
    GuaViewDraw(rootView);
    
    SDL_RenderPresent(renderer);
    
    return 0;
}

int
GuaGuiRun() {
    while (true) {
        _updataInput();
        _draw();
    }
    
    return 0;
}
