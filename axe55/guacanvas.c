#include <stdbool.h>

#include "guacanvas.h"

struct _GuaCanvasData;
typedef struct _GuaCanvasData GuaCanvasData;
struct _GuaCanvasData {
    SDL_Texture *texture;
    Uint32 *pixels;
    bool pressed;
};

static int
_draw(GuaCanvas *canvas) {
    GuaCanvas *view = canvas;
    
    GuaCanvasData *data = (GuaCanvasData *)canvas->data;
    
    Uint32 *p = data->pixels;
    SDL_Texture *t = data->texture;
    
    SDL_UpdateTexture(t, NULL, p, view->frame.w * sizeof(Uint32));
    SDL_RenderCopy(view->renderer, t, NULL, NULL);
    
    return 0;
}

static int
_onEvent(GuaView *view, GuaEvent event) {
    GuaCanvas *canvas = (GuaCanvas *)view;
    GuaCanvasData *data = (GuaCanvasData *)canvas->data;
    
    int mouseX = event.x;
    int mouseY = event.y;
    
    if (event.state == 1) {
        data->pressed = true;
        data->pixels[mouseY * (view->frame.w) + mouseX] = 0;
    } else if (event.state == 2) {
        data->pressed = false;
    } else if (event.state == 3) {
        if (data->pressed == true) {
            data->pixels[mouseY * (view->frame.w) + mouseX] = 0;
        }
    }
    
    return 0;
}

GuaCanvas *
GuaCanvasCreate(GuaRect frame, SDL_Renderer *renderer) {
    GuaView *canvas = GuaViewCreate(frame);
    canvas->draw = _draw;
    canvas->onEvent = _onEvent;
    canvas->renderer = renderer;
    
    GuaCanvasData *data = malloc(sizeof(GuaCanvasData));
    
    data->pressed = false;
    
    Uint64 cool[frame.w * frame.h];
    data->pixels = cool;
    
    memset(cool, 255, frame.w * frame.h * sizeof(Uint32));
    data->texture = SDL_CreateTexture(canvas->renderer, SDL_PIXELFORMAT_ARGB8888, SDL_TEXTUREACCESS_STATIC, frame.w, frame.h);
    canvas->data = (void *)data;
    
    return canvas;
}

