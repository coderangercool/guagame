#ifndef guaview_h
#define guaview_h

#include <stdio.h>
#include <stdbool.h>

#include <SDL2/SDL.h>
#include <SDL2_ttf/SDL_ttf.h>

#include "guaevent.h"

struct _GuaView;
typedef struct _GuaView GuaView;

struct _GuaVector2;
typedef struct _GuaVector2 GuaVector2;
struct _GuaVector2 {
    int x;
    int y;
};

struct _GuaRect;
typedef struct _GuaRect GuaRect;
struct _GuaRect {
    int x;
    int y;
    int w;
    int h;
};

bool GuaRectContainsPoint(GuaRect rect, GuaVector2 point);

struct _GuaColor;
typedef struct _GuaColor GuaColor;
struct _GuaColor {
    int r;
    int g;
    int b;
    int a;
};

typedef int (*GuaDraw) (GuaView *view);
typedef int (*GuaOnEvent) (GuaView *view, GuaEvent event);

struct _GuaView {
    GuaDraw draw;
    GuaOnEvent onEvent;
    
    GuaRect frame;
    GuaVector2 offset;
    GuaColor backgroundColor;
    
    GuaView *parent;
    GuaView *children;
    GuaView *next;
    GuaView *prev;
    
    SDL_Renderer *renderer;
    
    void *data;
};

GuaView *GuaViewCreate(GuaRect frame);

void GuaViewAdd(GuaView *parent, GuaView *view);

void GuaViewDraw(GuaView *view);

void GuaViewRemove(GuaView *view);

void GuaViewOnEvent(GuaView *view, GuaEvent event);


#endif /* guaview_h */
