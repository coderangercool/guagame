#ifndef guabutton_h
#define guabutton_h

#include <stdio.h>

#include "guaview.h"

typedef GuaView GuaButton;

typedef void (*GuaButtonAction)(GuaButton *Button);

GuaButton *GuaButtonCreate(GuaRect frame);

void GuaButtonSetTitle(GuaButton *button, const char *title);

void GuaButtonSetAction(GuaButton *button, GuaButtonAction action);

#endif /* guabutton_h */
