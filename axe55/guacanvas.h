#ifndef guacanvas_h
#define guacanvas_h

#include <stdio.h>

#include "guaview.h"

typedef GuaView GuaCanvas;

typedef void (*GuaCanvasAction)(GuaCanvas *canvas);

GuaCanvas *GuaCanvasCreate(GuaRect frame, SDL_Renderer *renderer);

void GuaCanvasSetAction(GuaCanvas *canvas, GuaCanvasAction action);

#endif /* guacanvas_h */
