from operator import add, sub, mul, truediv, mod
from enum import Enum

class Type(Enum):
    auto = 0            # auto 就是 6 个单字符符号, 用来方便写代码的
    colon = 1           # :
    comma = 2           # ,
    braceLeft = 3       # {
    braceRight = 4      # }
    bracketLeft = 5     # [
    bracketRight = 6    # ]
    number = 7          # 169
    string = 8          # "name"
    btrue = 9           # True
    bfalse = 10         # False
    bnull = 11          # Null
    functions = 12      # function
    operatorBool = 13   # <  > ! =
    operatorMath = 14   # + - * / %

class Token(object):
    def __init__(self, token_type, token_value):
        super(Token, self).__init__()
        # 用表驱动法处理 if
        d = {
            ':': Type.colon,
            ',': Type.comma,
            '{': Type.braceLeft,
            '}': Type.braceRight,
            '[': Type.bracketLeft,
            ']': Type.bracketRight,
        }
        if token_type == Type.auto:
            self.type = d[token_value]
        else:
            self.type = token_type
        self.value = token_value

    def __repr__(self):
        s = '"{}"'.format(self.value)
        return s


def string_end(code, index):
    """
    code = "abc"
    index = 1
    """
    s = ''
    offset = index
    trans = {
        '\"': '\"',
        '\t': '\t',
        '\n': '\n',
        '\\': '\\',
    }
    while offset < len(code):
        c = code[offset]
        if c == '"':
            # 找到了字符串的结尾
            # s = code[index:offset]
            return (s, offset)
        elif c == '\\':
            # 处理转义符, 现在只支持 \"
            # if code[offset+1] == '"':
            #     s += '"'
            #     offset += 2
            (s, offset) = transformToken(s, code, offset , trans)
            # else:
            #     # 这是一个错误, 非法转义符
            #     pass
        else:
            s += c
            offset += 1
    # 程序出错, 没有找到反引号 "
    pass

def transformToken(s, code, offset, trans):
    key = code[offset+1]
    if key in trans:
        s += trans[key]
        offset += 2
        return (s, offset)
    else:
        raise Exception('not supported token')

def s_tokens(code):
    length = len(code)
    tokens = []
    spaces = '\n\t\r '
    digits = '1234567890'
    opBool = '<>!='
    opMath = '+-*/%'
    # 当前下标
    i = 0
    while i < length:
        # 先看看当前应该处理啥
        c = code[i]
        i += 1
        if c in spaces:
            # 空白符号要跳过, space tab return
            continue
        elif c in ':,{}[]':
            # 处理 6 种单个符号
            t = Token(Type.auto, c)
            tokens.append(t)
        elif c in opBool:
            t = Token(Type.operatorBool, c)
            tokens.append(t)
        elif c in opMath:
            t = Token(Type.operatorMath, c)
            tokens.append(t)
        elif c == '"':
            # 处理字符串
            s, offset = string_end(code, i)
            i = offset + 1
            # print('i, offset', i, offset, s, code[offset])
            t = Token(Type.string, s)
            tokens.append(t)
        elif c in digits:
            # 处理数字, 现在不支持小数和负数
            end = 0
            for offset, char in enumerate(code[i:]):
                if char not in digits:
                    end = offset
                    break
            n = code[i-1:i+end]
            i += end
            t = Token(Type.number, int(n))
            tokens.append(t)
        elif ord(c) in range(65, 91) or ord(c) in range(97, 123):
            offset = i
            f = str(c)
            while offset < len(code):
                t = code[offset]
                if not (ord(t) in range(65, 91) or ord(t) in range(97, 123)):
                    i = offset + 1
                    t = Token(Type.functions, f)
                    tokens.append(t)
                    break
                else:
                    f += code[offset]
                    offset += 1
        elif (c + code[i] + code[i+1] == 'yes') and code[i+2] in spaces:
            t = Token(Type.btrue, True)
            i += 3
            tokens.append(t)
        elif (c + code[i] == 'no') and code[i+1] in spaces:
            i += 3
            t = Token(Type.bfalse, False)
            tokens.append(t)
        # elif c == 'n':
        #     i += 3
        #     t = Token(Type.bnull, None)
        #     tokens.append(t)
        else:
            # 出错了
            pass
    result = [t.value for t in tokens]
    return result

def parsed_ast(token_list):
    """
    递归解析 ast
    """
    ts = token_list
    token = ts[0]
    del ts[0]
    if token == '[':
        exp = []
        while ts[0] != ']':
            t = parsed_ast(ts)
            exp.append(t)
        # 循环结束, 删除末尾的 ']'
        del ts[0]
        return exp
    else:
        # token 需要 process_token / parsed_token
        return token

def pop_list(stack):
    l = []
    while stack[-1] != '[':
        l.append(stack.pop(-1))
    stack.pop(-1)
    l.reverse()
    return l


def parsed_ast_stack(token_list):
    """
    用栈解析 ast
    """
    l = []
    i = 0
    while i < len(token_list):
        token = token_list[i]
        i += 1
        if token == ']':
            list_token = pop_list(l)
            l.append(list_token)
        else:
            l.append(token)
    return l

def reduce(fn, l, vars):
    for i, e in enumerate(l):
        if not isinstance(e, (int, float)):
           if e in vars:
               l[i] = vars[e]
           else:
               raise Exception('undefined variable')
    reduced = l[0]
    rest = l[1:]
    for x in rest:
        reduced = fn(reduced, x)
    return reduced

def applyMath(tokens, vars):
    first = tokens[0]
    rest = tokens[1:]
    fnMath = {
        "+": add,
        "-": sub,
        "*": mul,
        "/": truediv,
        '%': mod
    }
    return reduce(fnMath[first], rest, vars)

def apply(code, vars):
    opBool = '<>!='
    opMath = '+-*/%'
    ts = s_tokens(code)
    # print(ts)
    ast = parsed_ast_stack(ts)
    # print(ast)
    for l in ast:
        if l[0] == 'set':
            vars[l[1]] = l[2]
        if l[0] in opMath:
            res = applyMath(l, vars)
            return res





def main():
    code1 = '''
            [set a 1]
            [set b 2]
            [+ a b ]
            '''
    res1 = apply(code1, {})
    code2 = '''
            [set a 2]
            [set b 3]
            [+ a b ]
            '''
    res2 = apply(code2, {})
    code3 = '''
            [set a 1]
            [set b 2]
            [set c 3]
            [+ a b c ]
            '''
    res3 = apply(code3, {})
    assert res1 == 3
    assert res2 == 5
    assert res3 == 6


if __name__ == '__main__':
    main()