#ifndef switch_h
#define switch_h

#include <stdio.h>
#include <stdlib.h>
#include <SDL2/SDL.h>

#endif /* switch_h */

#include "button.h"

struct _slider;
typedef struct _slider GuaSlider;

GuaSlider *GuaNewSlider(int x, int y, int wback, int wfront, int h);

int GuaSliderDrag(GuaSlider *sld, int x);
