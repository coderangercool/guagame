#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <SDL2/SDL.h>

#include "slider.h"
#include "button.h"

struct _slider {
    GuaButton *back;
    GuaButton *front;
    int status;
};

struct _button {
    int x;
    int y;
    int w;
    int h;
    void* action;
};

GuaSlider *GuaNewSlider(int x, int y, int wback, int wfront, int h) {
    GuaSlider *sld = malloc(sizeof(GuaSlider));
    GuaButton *back = malloc(sizeof(GuaButton));
    GuaButton *front = malloc(sizeof(GuaButton));
    back->x = x;
    back->y = y;
    back->w = wback;
    back->h = h;
    front->x = x;
    front->y = y;
    front->w = wfront;
    front->h = h;
    sld->back = back;
    sld->front = front;
    sld->status = 0;
    
    return sld;
}

int
GuaSliderDrag(GuaSlider *sld, int x) {
    int xx = sld->back->x + sld->back->w;
    if (x > sld->back->x && x < xx) {
        sld->front->x = x;
    }
    return 0;
}
