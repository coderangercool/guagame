#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <SDL2/SDL.h>

#include "input.h"

struct _input {
    int x;
    int y;
    int w;
    int h;
    char *inputText;
};

GuaInput *
GuaInputNew(int x, int y, int w, int h, char *inputText) {
    GuaInput *input = malloc(sizeof(GuaInput));
    
    input->x = x;
    input->y = y;
    input->w = w;
    input->h = h;
    input->inputText = inputText;
    
    return input;
}
