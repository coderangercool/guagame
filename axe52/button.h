#ifndef button_h
#define button_h

#include <stdio.h>
#include <stdlib.h>
#include <SDL2/SDL.h>

#endif /* button_h */

struct _button;
typedef struct _button GuaButton;

GuaButton* GuaButtonNew(int posX, int posY, int width, int height);

int GuaButtonSetAction(GuaButton* button, void* actionClick);




