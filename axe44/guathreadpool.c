#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <unistd.h>

#include "guathreadpool.h"


struct _Task;
typedef struct _Task GuaTask;


struct _TaskQueue;
typedef struct _TaskQueue GuaTaskQueue;
// GuaTaskQueue 的定义和接口你自己来根据需要补全

//struct _Condition;
//typedef struct _Condition GuaCondition;

struct _Task {
    GuaTaskCallback *callback;
    void *arg;                      //回调函数参数
    GuaTask *next;
};

struct _TaskQueue {
    GuaTask *first;
    GuaTask *last;
    int queue_count;
};

//struct _Condition {
//    pthread_mutex_t p_mutex;
//    pthread_cond_t p_cond;
//};


struct _GuaThreadPool {
    int numberOfThreads;        // 线程池中的线程数量
    pthread_t *threads;         // 线程池中所有线程的 id
    // 补全你需要的其他变量
    GuaTask *first;
    pthread_mutex_t queue_lock;
    pthread_cond_t  queue_ready;
    int quit;
};


// 线程池里面的单个线程的入口函数
void *_GuaThreadPoolThreadNew(void *arg);

static GuaThreadPool *pool = NULL;

GuaThreadPool*
GuaThreadPoolNew(int numberOfThreads) {
    int i;
    pool = calloc(1, sizeof(GuaThreadPool));
    if (!pool) {
        exit(1);
    }
    pool->numberOfThreads = numberOfThreads;
    pool->quit = 0;
    pool->first = NULL;
    pool->threads = calloc(numberOfThreads, sizeof(pthread_t));
    for (i = 0; i < numberOfThreads; i++) {
        if (pthread_create(&pool->threads[i], NULL, _GuaThreadPoolThreadNew, NULL) != 0) {
            exit(1);
        }
    }
    return pool;
}

int
GuaThreadPoolAddTask(GuaThreadPool* pool, GuaTaskCallback *callback, void *arg) {
    GuaTask *task;
    task = malloc(sizeof(GuaTask));
    task->callback = callback;
    task->arg = arg;
    task->next = NULL;
    GuaTask *member;
    pthread_mutex_lock(&pool->queue_lock);
    member = pool->first;
    if (!member) {
        pool->first = task;
    } else {
        while (member->next) {
            member = member->next;
        }
        member->next = task;
    }
    pthread_cond_signal(&pool->queue_ready);
    pthread_mutex_unlock(&pool->queue_lock);
    return 0;
}

void *
_GuaThreadPoolThreadNew(void* arg) {
    GuaTask *task;
    while (1) {
        pthread_mutex_lock(&pool->queue_lock);
        while (!pool->first && !pool->quit) {
            pthread_cond_wait(&pool->queue_ready, &pool->queue_lock);
        }
        if (pool->quit) {
            pthread_mutex_unlock(&pool->queue_lock);
            pthread_exit(NULL);
        }
        task = pool->first;
        pool->first = pool->first->next;
        pthread_mutex_unlock(&pool->queue_lock);
        task->callback(task->arg);
        free(task);
    }
    return NULL;
}

int
GuaThreadPoolFree(GuaThreadPool *pool) {
    int i;
    GuaTask *member;
    if (pool->quit) {
        return 0;
    }
    pool->quit = 1;
    
    pthread_mutex_lock(&pool->queue_lock);
    pthread_cond_broadcast(&pool->queue_ready);
    pthread_mutex_unlock(&pool->queue_lock);
    for (i = 0; i < pool->numberOfThreads; i++) {
        pthread_join(pool->threads[i], NULL);
    }
    free(pool->threads);
    
    while (pool->first) {
        member = pool->first;
        pool->first = pool->first->next;
        free(member);
    }
    pthread_mutex_destroy(&pool->queue_lock);
    pthread_cond_destroy(&pool->queue_ready);
    
    free(pool);
    return 1;
}

