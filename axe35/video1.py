import numpy as np
import matplotlib.pyplot as plot
from PIL import Image
import copy
import json
import sys


def grayimage(path):
    # convert('L') 转为灰度图
    # 这样每个像素点就只有一个灰度数据
    img = Image.open(path).convert('L')
    return img


def encode(img1, img2):
    result = []
    dictionary = {
        'x': -1,
        'y': -1,
        'data': []
    }
    w, h = img1.size
    m, n = w // 8, h // 8
    pixels1 = img1.load()
    pixels2 = img2.load()
    exit_sign = False
    for y in range(n):
        for x in range(m):
            found = False
            if x == 0 or y == 0:
                d = copy.deepcopy(dictionary)
                i = x * 8
                j = y * 8
                for jj in range(j, j + 8):
                    for ii in range(i, i + 8):
                        d['data'].append(pixels2[ii, jj])
                result.append(d)
            else:
                for yy in [y - 1, y, y + 1]:
                    if yy >= y:
                        continue
                    if exit_sign is True:
                        exit_sign = False
                        break
                    for xx in [x - 1, x, x + 1]:
                        if xx >= x:
                            continue
                        if exit_sign is True:
                            break
                        sump = 0
                        xxx = xx * 8
                        yyy = yy * 8
                        for j in range(yyy, yyy + 8):
                            if exit_sign is True:
                                break
                            jj = (j - yyy) + (y * 8)
                            for i in range(xxx, xxx + 8):
                                ii = (i - xxx) + (x * 8)
                                sump += abs(pixels2[ii, jj] - pixels1[i, j])
                        print(sump)
                        if sump < 20:
                            d = copy.deepcopy(dictionary)
                            d['x'] = xxx
                            d['y'] = yyy
                            d['data'] = None
                            result.append(d)
                            exit_sign = True
                            found = True
                            break
                if found is False:
                    d = copy.deepcopy(dictionary)
                    i = x * 8
                    j = y * 8
                    for jj in range(j, j + 8):
                        for ii in range(i, i + 8):
                            d['data'].append(pixels2[ii, jj])
                    result.append(d)
    return result


def decode(img, data):
    w, h = img.size
    m, n = w // 8, h // 8
    pixels = img.load()
    for y in range(n):
        for x in range(m):
            index = y * m + x
            d = data[index]
            p = data[index]['data']
            if d['x'] == -1:
                i = x * 8
                j = y * 8
                for jj in range(j, j + 8):
                    for ii in range(i, i + 8):
                        pidx = (jj - j) * 8 + ii - i
                        pixels[ii, jj] = p[pidx]
            elif p is None:
                i = x * 8
                j = y * 8
                ti = d['x']
                tj = d['y']

                for jj in range(j, j + 8):
                    tti = ti
                    for ii in range(i, i + 8):
                        pixels[ii, jj] = pixels[tti, tj]
                        tti = tti + 1
                    tj = tj + 1
    img.save('decode.png')


#
# def main():
#     path1 = 'big_buck_bunny_07501.png'
#     path2 = 'big_buck_bunny_07502.png'
#     img1 = grayimage(path1)
#     img2 = grayimage(path2)
#
#     # 下面是一段遍历像素并操作的范例
#     # 供参考
#     w, h = img1.size
#     threshold = 128
#     pixels = img1.load()
#     for y in range(h):
#         for x in range(w):
#             if pixels[x, y] < threshold:
#                 pixels[x, y] = 0
#             else:
#                 pixels[x, y] = 255
#     img1.save('保存图片样例.png')


def main():
    path1 = 'big_buck_bunny_07501.png'
    path2 = 'big_buck_bunny_07502.png'
    patho = 'b.videoblock'
    img1 = grayimage(path1)
    img2 = grayimage(path2)

    if sys.argv[1] == 'encode':
        json_data = json.dumps(encode(img1, img2))
        file = open(patho, 'w')
        file.write(json_data)
        file.close()

    if sys.argv[1] == 'decode':
        file = open(patho, 'r')
        content = file.read()
        data = json.loads(content)
        file.close()
        decode(img1, data)


if __name__ == '__main__':
    main()

# 0.学习重在掌握原理，学会之后替换算法很容易。
# 1.将变化大的帧作为关键帧，其后的画面在此基础上加上差值信息动态计算，直到下一个关键帧。
