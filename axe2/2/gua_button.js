class GuaButton extends GuaObject{
    constructor() {
        super()
        this.position = GuaPoint.new(0, 0)
        this.size = GuaSize.new(0, 0)
        this.color = GuaColor.red()
        this.cvs = null
        this.eventList = []
    }

    addAction(callback) {
        this.eventList.push(callback)
    }

    observeAction() {
        this.cvs.canvas.addEventListener('click', (e) => {
            if (this.isInside(e)) {
                this.eventList.forEach(item => item(e))
            }
        })
    }

    isInside(e) {
        let [x, y] = [e.offsetX, e.offsetY]
        let [lx, rx] = [this.position.x, this.position.x + this.size.w]
        let [ty, by] = [this.position.y, this.position.y + this.size.h]
        return x > lx && x < rx && y > ty && y < by
    }

    setColor(e, color) {
        if (this.isInside(e)) {
            let {x, y} = this.position
            let fillPoint = GuaPoint.new(x + 1, y + 1)
            let {w, h} = this.size
            let fillSize = GuaSize.new(w - 1, h - 1)
            this.cvs.fill(fillPoint, fillSize, color)
        }
    }

    colorChange() {
        let self = this
        this.cvs.canvas.addEventListener('mousedown', function (e) {
            self.setColor(e, self.color)
        })
        this.cvs.canvas.addEventListener('mouseup', function (e) {
            self.setColor(e, GuaColor.white())
        })
    }

    setup() {
        this.cvs.drawRect(this.position, this.size, GuaColor.white())
        this.colorChange()
        this.observeAction()
    }
}