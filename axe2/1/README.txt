作业 2
因为国庆假期, 我们的课程跳过 7 天
原定周日(10.1)的课移到下周日(10.8)上, 具体时间会另行通知
作业截止时间:
10.8 日下午 13:00 前
交作业方式:
在项目中新建 axe2 目录
在 axe2 目录中新建 1 目录和 2 目录
1 中存放 C 语言的文件
2 中存放 js 的文件
有什么去 #question 频道按规范提问
C 语言作业
实现 O(1) 时间复杂度的 GuaListLength, 可以更改 GuaList 的内部实现
实现 O(1) 时间复杂度的 GuaListAppend, 可以更改 GuaList 的内部实现
利用 GuaList 实现 GuaStack, 包含以下方法(注意时间复杂度的要求)
创建 2 个新文件 GuaStack.h 和 GuaStack.c
GuaStackCreate()            创建并返回一个空栈
GuaStackLength()            时间复杂度 O(1)
GuaStackPush(type e)        时间复杂度 O(1)
GuaStackPop()               时间复杂度 O(1), 返回栈顶的元素
GuaStackIsEmpty()           时间复杂度 O(1), 检查栈是否为空
GuaStackClear()             时间复杂度 O(n), 删除栈的所有元素(清空栈)
GuaStackRemove(GuaStack *)  删除栈中所有元素并销毁栈
利用 GuaList 实现 GuaQueue, 包含以下方法(注意时间复杂度的要求)
创建 2 个新文件 GuaQueue.h 和 GuaQueue.c
GuaQueueCreate()            创建并返回一个空队列
GuaQueueLength()            时间复杂度 O(1)
GuaQueueEnqueue(type e)     时间复杂度 O(1), 把元素 e 放入队列
GuaQueueDequeue()           时间复杂度 O(1), 元素出队
GuaQueueIsEmpty()           时间复杂度 O(1), 检查队列是否为空
GuaQueueClear()             时间复杂度 O(n), 删除队列的所有元素(清空队列)
GuaQueueRemove(GuaQueue *)  删除队列中所有元素并销毁队列
js 作业
电脑屏幕显示的内容都是存在显存中的
显存相当于一个大数组, 操作系统通过设置显存来让屏幕显示不同的内容
现在的电脑都是 32 位色, 也就是说一个像素 4 个字节分别表示 rgba
所以你可以把 canvas 的 data 当作显存来看待(原理是一模一样的, 相当于 canvas 是屏幕)
我们课程的这一部分是从 0 开始制作一套图形界面系统(GUI), 通过 drawPoint 来实现
然后在之后的课程中利用这一套自制的图形界面系统来实现窗口/视图/按钮/输入框等等
之前的鼠标拖拽画线和画矩形是小练习, 为了实现 GUI, 必须使用直播中写游戏的的层级模式
这一套功能用任何语言都能实现, 选择 js 的理由是环境方便开发轻松, 可以让我们专心抓主要矛盾
所以虽然是在用高级的语言, 但是做的是底层的原理方面的事
1, 实现 button 的功能, 定义一个 GuaButton 类, 用 canvas.addElement 函数添加 button 到画面中
GuaButton 有 addAction(clickCallback) 方法可以添加点击回调函数
具体会进一步给出演示用的 gif 图

