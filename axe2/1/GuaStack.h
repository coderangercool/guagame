//
//  GuaStack.h
//  axe


#ifndef GuaStack_h
#define GuaStack_h

#include <stdio.h>
#include <stdbool.h>

typedef struct GuaNode GuaStack;

//创建一个空栈
GuaStack *
GuaStackCreate(void);

//栈长度
int
GuaStackLength(GuaStack *stack);

void
GuaStackPush(GuaStack *stack, type e);        //时间复杂度 O(1)

int
GuaStackPop(GuaStack *stack);               //时间复杂度 O(1), 返回栈顶的元素

bool
GuaStackIsEmpty(GuaStack *stack);           //时间复杂度 O(1), 检查栈是否为空

void
GuaStackClear(GuaStack *stack);             //时间复杂度 O(n), 删除栈的所有元素(清空栈)

void
GuaStackRemove(GuaStack *stack);  //删除栈中所有元素并销毁栈

#endif /* GuaStack_h */
