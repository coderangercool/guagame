#include <stdio.h>
#include <stdbool.h>

#include "GuaList.h"
#include "GuaStack.h"
#include "GuaQueue.h"
#include "GuaTest.h"


void
testGuaListLength() {
    type a1[] = {1, 2, 3};
    int n1 = 3;
    GuaList *l1 = GuaListCreate(a1, n1);
    ensure(GuaListLength(l1) == n1, "test list length 1");

    type a2[] = {};
    int n2 = 0;
    GuaList *l2 = GuaListCreate(a2, n2);
    ensure(GuaListLength(l2) == n2, "test list length 2");
    
    type a3[] = {1, 2, 3, 4, 5};
    int n3 = 5;
    GuaList *l3 = GuaListCreate(a3, n3);
    ensure(GuaListLength(l3) == n3, "test list length 3");
}

void
testGuaListContains() {
    type a[] = {1, 2, 3, 4, 5};
    int n = 5;
    GuaList *l = GuaListCreate(a, n);
    int e1 = 1;
    ensure(GuaListContains(l, e1) == true, "test list contains 1");
    
    int e2 = 2;
    ensure(GuaListContains(l, e2) == true, "test list contains 2");
    
    int e3 = 0;
    ensure(GuaListContains(l, e3) == false, "test list contains 3");
}

void testGuaListAppend() {
    type a[] = {1, 2, 3};
    int n = 3;
    GuaList *l =  GuaListCreate(a, n);
    int e1 = 4;
    GuaListAppend(l, e1);
    ensure(GuaListLength(l) == 4, "test list append 1");
    
    int e2 = 5;
    GuaListAppend(l, e2);
    ensure(GuaListLength(l) == 5, "test list append 2");
    
    int e3 = 6;
    GuaListAppend(l, e3);
    ensure(GuaListLength(l) == 6, "test list append 3");
    
}


void testGuaListPrepend() {
    type a[] = {1, 2, 3};
    int n = 3;
    GuaList *l = GuaListCreate(a, n);
    
    int e1 = 1;
    GuaListPrepend(l, e1);
    ensure(GuaListLength(l) == 4, "test list prepend 1");
    
    int e2 = 2;
    GuaListPrepend(l, e2);
    ensure(GuaListLength(l) == 5, "test list prepend 2");
    
    
    int e3 = 3;
    GuaListPrepend(l, e3);
    ensure(GuaListLength(l) == 6, "test list prepend 3");
}

void
testGuaListIndexOfElement() {
    type a[] = {1, 2, 3};
    int n = 3;
    GuaList *l = GuaListCreate(a, n);
    
    int e1 = 1;
    ensure(GuaListIndexOfElement(l, e1) == 0, "text index of element 1");
    
    int e2 = 2;
    ensure(GuaListIndexOfElement(l, e2) == 1, "text index of element 2");
    
    int e3 = 5;
    ensure(GuaListIndexOfElement(l, e3) == -1, "text index of element 3");
}

void
testGuaListInsertElementAtIndex() {
    type a[] = {1, 2, 3};
    int n = 3;
    GuaList *l = GuaListCreate(a, n);
    int e1 = 5;
    int p1 = 1;
    GuaListInsertElementAtIndex(l, e1, p1);
    ensure(GuaListIndexOfElement(l, e1) == p1, "text list insert 1");
    
    int e2 = 6;
    int p2 = 2;
    GuaListInsertElementAtIndex(l, e2, p2);
    ensure(GuaListIndexOfElement(l, e2) == p2, "text list insert 2");
    
    int e3 = 7;
    int p3 = 2;
    GuaListInsertElementAtIndex(l, e3, p3);
    ensure(GuaListIndexOfElement(l, e3) == p3, "text list insert 3");
}

void
testGuaStackLength() {
    GuaStack *stack = GuaStackCreate();
    ensure(GuaStackLength(stack) == 0, "test stack length 1" );
    
    int e1 = 1, e2 = 2, e3 = 3;
    GuaStackPush(stack, e1);
    GuaStackPush(stack, e2);
    GuaStackPush(stack, e3);
    ensure(GuaStackLength(stack) == 3, "test stack length 2" );
    
    int e4 = -1, e5 = -2;
    GuaStackPush(stack, e4);
    GuaStackPush(stack, e5);
    ensure(GuaStackLength(stack) == 5, "test stack length 3" );
}

void
testGuaStackPop() {
    GuaStack *stack = GuaStackCreate();
    int e1 = 1, e2 = 3;
    GuaStackPush(stack, e1);
    GuaStackPush(stack, e2);
    
    ensure(GuaStackPop(stack) == 3, "test stack pop 1");
    
    ensure(GuaStackPop(stack) == 1, "test stack pop 2");
    
    ensure(GuaStackPop(stack) == 0, "test stack pop 3");
}

void
testGuaStackIsEmpty() {
    GuaStack *stack = GuaStackCreate();
    
    ensure(GuaStackIsEmpty(stack) == true, "test stack is empty 1");
    
    int e1 = 1, e2 = 2;
    GuaStackPush(stack, e1);
    GuaStackPush(stack, e2);
    
    ensure(GuaStackIsEmpty(stack) == false, "test stack is empty 2");
}

void
testGuaStackClear() {
    GuaStack *stack = GuaStackCreate();
    int e1 = 1, e2 = 2;
    GuaStackPush(stack, e1);
    GuaStackPush(stack, e2);
    
    GuaStackClear(stack);
    
    ensure(GuaStackLength(stack) == 0, "test stack clear 1");
    
    int e3 = 3;
    GuaStackPush(stack, e3);
    ensure(GuaStackLength(stack) == 1, "test stack clear 2");
    
    GuaStackClear(stack);
    ensure(GuaStackLength(stack) == 0, "test stack clear 3");
}


void
testGuaQueueLength() {
    GuaQueue *queue = GuaQueueCreate();
    ensure(GuaQueueLength(queue) == 0, "test queue length 1" );
    
    int e1 = 1, e2 = 2, e3 = 3;
    GuaQueueEnqueue(queue, e1);
    GuaQueueEnqueue(queue, e2);
    GuaQueueEnqueue(queue, e3);
    ensure(GuaQueueLength(queue) == 3, "test queue length 2" );
    
    int e4 = -1, e5 = -3;
    GuaQueueEnqueue(queue, e4);
    GuaQueueEnqueue(queue, e5);
    ensure(GuaQueueLength(queue) == 5, "test queue length 3" );
}

void
testGuaQueueDequeue() {
    GuaQueue *queue = GuaQueueCreate();
    int e1 = 1, e2 = 3;
    GuaQueueEnqueue(queue, e1);
    GuaQueueEnqueue(queue, e2);
    
    ensure(GuaQueueDequeue(queue) == 1, "test queue dequeue 1");
    
    ensure(GuaQueueDequeue(queue) == 3, "test queue dequeue 2");
    
    ensure(GuaQueueDequeue(queue) == 0, "test queue dequeue 3");
}


void
testGuaQueueIsEmpty() {
    GuaQueue *queue = GuaQueueCreate();
    
    ensure(GuaQueueIsEmpty(queue) == true, "test queue is empty 1");
    
    int e1 = 1, e2 = 2;
    GuaQueueEnqueue(queue, e1);
    GuaQueueEnqueue(queue, e2);
    
    ensure(GuaQueueIsEmpty(queue) == false, "test queue is empty 2");
}

void
testGuaQueueClear() {
    GuaQueue *queue = GuaQueueCreate();
    int e1 = 1;
    GuaQueueEnqueue(queue, e1);
    
    GuaQueueClear(queue);
    
    ensure(GuaQueueLength(queue) == 0, "test queue clear 1");
    
    int e3 = 5;
    int e5 = 6;
    GuaQueueEnqueue(queue, e3);
    GuaQueueEnqueue(queue, e5);
    ensure(GuaQueueLength(queue) == 2, "test queue clear 2");
    
    GuaQueueClear(queue);
   
    ensure(GuaQueueLength(queue) == 0, "test queue clear 3");
}

int
main(int argc, const char * argv[]) {
    //    测试GuaList相关函数
    testGuaListLength();
    testGuaListContains();
    testGuaListAppend();
    testGuaListPrepend();
    testGuaListIndexOfElement();
    testGuaListInsertElementAtIndex();
    
    //    测试GuaStack相关函数
    testGuaStackLength();
    testGuaStackPop();
    testGuaStackIsEmpty();
    testGuaStackClear();
    
    //    测试GuaQueue相关函数
    testGuaQueueLength();
    testGuaQueueDequeue();
    testGuaQueueIsEmpty();
    testGuaQueueClear();
    
    // 返回 0 表示程序执行完毕并成功退出
    return 0;
}
