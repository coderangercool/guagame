//
//  GuaQueue.c
//  axe


#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "GuaList.h"
#include "GuaQueue.h"

struct GuaNode {
    type element;
    int size;
    GuaList *next;
    GuaList *base;
};


GuaQueue *
GuaQueueCreate(void) {
    GuaList *n = malloc(sizeof(GuaList));
    n->next = NULL;
    n->base = n;
    return n;
}

int
GuaQueueLength(GuaQueue *queue) {
    return GuaListLength(queue);
}

void
GuaQueueEnqueue(GuaQueue *queue, type e) {
    GuaListAppend(queue, e);
}

int
GuaQueueDequeue(GuaQueue *queue) {
    GuaList *n = malloc(sizeof(GuaList));
    n = queue->next;
    if (n == NULL) {
        return 0;
    }
    int data = n->element;
    queue->next = n->next;
    queue->size--;
//    printf("size %d \n", queue->size);
    return data;
}

bool
GuaQueueIsEmpty(GuaQueue *queue) {
    if (queue->next == NULL) {
        return true;
    } else {
        return false;
    }
}

void
GuaQueueClear(GuaQueue *queue) {
    while (queue->next != NULL) {
        GuaQueueDequeue(queue);
    }
    queue->base = queue;
}

void
GuaQueueRemove(GuaQueue *queue) {
    GuaQueueDequeue(queue);
    free(queue);
    queue = NULL;
}
