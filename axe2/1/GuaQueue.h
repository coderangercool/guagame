//
//  GuaQueue.h
//  axe

#ifndef GuaQueue_h
#define GuaQueue_h

#include <stdio.h>
#include <stdbool.h>

typedef struct GuaNode GuaQueue;

GuaQueue *
GuaQueueCreate(void);            //创建并返回一个空队列

int
GuaQueueLength(GuaQueue *queue);            //时间复杂度 O(1)

void
GuaQueueEnqueue(GuaQueue *queue, type e);     //时间复杂度 O(1), 把元素 e 放入队列

int
GuaQueueDequeue(GuaQueue *queue);           //时间复杂度 O(1), 元素出队

bool
GuaQueueIsEmpty(GuaQueue *queue);           //时间复杂度 O(1), 检查队列是否为空

void
GuaQueueClear(GuaQueue *queue);             //时间复杂度 O(n), 删除队列的所有元素(清空队列)

void
GuaQueueRemove(GuaQueue *queue);  //删除队列中所有元素并销毁队列

#endif /* GuaQueue_h */
