//
//  GuaStack.c
//  axe

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include "GuaList.h"
#include "GuaStack.h"

struct GuaNode {
    type element;
    int size;
    GuaList *next;
    GuaList *base;
};

GuaStack *
GuaStackCreate() {
    GuaList *n = malloc(sizeof(GuaList));
    n->next = NULL;
    return n;
}

int
GuaStackLength(GuaStack *stack){
    return GuaListLength(stack);
}

void
GuaStackPush(GuaStack *stack, type e){
    GuaListPrepend(stack, e);
}

int
GuaStackPop(GuaStack *stack){
    GuaList *n = malloc(sizeof(GuaList));
    n = stack->next;
    if (n == NULL) {
        return 0;
    }
    int data = n->element;
    stack->next = n->next;
    stack->size--;
    return data;
}

bool
GuaStackIsEmpty(GuaStack *stack){
    if (stack->next == NULL) {
        return true;
    } else {
        return false;
    }
}

void
GuaStackClear(GuaStack *stack) {
    while (stack->next != NULL) {
        GuaStackPop(stack);
    }
}

void
GuaStackRemove(GuaStack *stack){
    GuaStackClear(stack);
    free(stack);
    stack = NULL;
}

