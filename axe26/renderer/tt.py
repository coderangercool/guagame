class Matrix(object):
    def __init__(self, matrix_list=None):
        if matrix_list is not None:
            self.m = matrix_list
        else:
            self.m = [
                [0, 0, 0, 0],
                [0, 0, 0, 0],
                [0, 0, 0, 0],
                [0, 0, 0, 0],
            ]

    def __getitem__(self, item):
        """
        :type item: tuple
        """
        i, j = item
        return self.m[i][j]

    def __setitem__(self, key, value):
        """
        :type key: tuple
        """
        i, j = key
        self.m[i][j] = value

    def __eq__(self, other):
        return str(self) == str(other)

    def __str__(self):
        s = ''.join(['{0:.3f}'.format(self[i // 4, i % 4]) + ('\n' if i % 4 == 3 else ' ')
                    for i in range(16)])
        return s

m = Matrix()
print(m)