// const GuaObject = require('./object')
// const Vector = require('./vector')

class Matrix extends GuaObject{
    constructor(matrixList) {
        super()
        if (matrixList) {
            this.m = matrixList
        } else {
            this.m = [
                [0, 0, 0, 0],
                [0, 0, 0, 0],
                [0, 0, 0, 0],
                [0, 0, 0, 0],
            ]
        }
    }

    get(item)  {
        let [i,j] = item
       return this.m[i][j]
    }

    set(key, value) {
        let [i, j] = key
        this.m[i][j] = value
    }

    equalM(other) {
        return this.toString() === other.toString()
    }

    toString() {
        let s = ''
        for (let i = 0; i < 16; i++) {
            if (this.m[Math.floor(i / 4)][i % 4]) {
                s += this.m[Math.floor(i / 4)][i % 4].toFixed(3)
            } else {
                s += this.m[Math.floor(i / 4)][i % 4]
            }

            if (i % 4 === 3) {
                s += '\n'
            } else {
                s += ' '
            }
        }
        return s
    }

    mul(other) {
        let m1 = this
        let m2 = other
        let m = new Matrix()
        for (let i = 0; i < 16; i++) {
            let e = Math.floor(i / 4)
            let j = i % 4
            m.m[e][j] = m1.m[e][0] * m2.m[0][j] + m1.m[e][1] * m2.m[1][j] + m1.m[e][2] * m2.m[2][j] + m1.m[e][3] * m2.m[3][j]
        }
        return m
    }

    static zero() {
        return new Matrix()
    }

    static identity() {
        let m = [
            [1, 0, 0, 0],
            [0, 1, 0, 0],
            [0, 0, 1, 0],
            [0, 0, 0, 1],
        ]
        return new Matrix(m)
    }

    static lookAtLH(eye, target, up) {
        for (let key of Object.keys(target)) {
            target[key] = target[key] - eye[key]
        }
        log('target', target)
        let zaxis = target.normalize()
        let xaxis = up.cross(zaxis).normalize()
        let yaxis = zaxis.cross(xaxis).normalize()

        let ex = -xaxis.dot(eye)
        let ey = -yaxis.dot(eye)
        let ez = -zaxis.dot(eye)

        let m = [
            [xaxis.x, yaxis.x, zaxis.x, 0],
            [xaxis.y, yaxis.y, zaxis.y, 0],
            [xaxis.z, yaxis.z, zaxis.z, 0],
            [ex, ey, ez, 1]
        ]
        return new Matrix(m)
    }
    
    static perspectiveFovLH(field_of_view, aspect, znear, zfar) {
        let h = 1 / Math.tan(field_of_view / 2)
        let w = h / aspect
        let m = [
            [w, 0, 0, 0],
            [0, h, 0, 0],
            [0, 0, zfar / (zfar - znear), 1],
            [0, 0, (znear * zfar) / (znear - zfar), 0],
        ]
        return new Matrix(m)
    }

    static rotationX(angle) {
        let s = Math.sin(angle)
        let c = Math.cos(angle)
        let m = [
            [1, 0, 0, 0],
            [0, c, s, 0],
            [0, -s, c, 0],
            [0, 0, 0, 1]
        ]
        return new Matrix(m)
    }
    
    static rotationY(angle) {
        let s = Math.sin(angle)
        let c = Math.cos(angle)
        let m = [
            [c, 0, -s, 0],
            [0, 1, 0, 0],
            [s, 0, c, 0],
            [0, 0, 0, 1]
        ]
        return new Matrix(m)
    }

    static rotationZ(angle) {
        let s = Math.sin(angle)
        let c = Math.cos(angle)
        let m = [
            [c, s, 0, 0],
            [-s, c, 0, 0],
            [0, 0, 1, 0],
            [0, 0, 0, 1]
        ]
        return new Matrix(m)
    }

    static rotation(angle) {
        let mz = Matrix.rotationZ(angle.z)
        let mx = Matrix.rotationX(angle.x)
        let my = Matrix.rotationY(angle.y)

        let mmatrix = mz.mul(mx).mul(my)
        return mmatrix
    }
    
    static translation(vector) {
        let v = vector
        let [x, y, z] = [v.x, v.y, v.z];
        let m = [
            [1, 0, 0, 0],
            [0, 1, 0, 0],
            [0, 0, 1, 0],
            [x, y, z, 1],
        ]
        return new Matrix(m)
    }

    transformVector(vector) {
        let v = vector
        let m = this
        let x = v.x * m.m[0][0] + v.y * m.m[1][0] + v.z * m.m[2][0] + m.m[3][0]
        let y = v.x * m.m[0][1] + v.y * m.m[1][1] + v.z * m.m[2][1] + m.m[3][1]
        let z = v.x * m.m[0][2] + v.y * m.m[1][2] + v.z * m.m[2][2] + m.m[3][2]
        let w = v.x * m.m[0][3] + v.y * m.m[1][3] + v.z * m.m[2][3] + m.m[3][3]

        return new GuaVector(x / w, y / w, z / w)
    }
}


// module.exports = Matrix