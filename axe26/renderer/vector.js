// const GuaObject = require('./object')

class GuaVector extends GuaObject {
    // 表示三维点的类
    constructor(x, y, z) {
        super()
        this.x = x
        this.y = y
        this.z = z
    }

    length() {
        return Math.sqrt(Math.pow(this.x, 2) + Math.pow(this.y, 2) + Math.pow(this.z, 2))
    }

    normalize() {
        let l = this.length()
        if (l === 0) {
            return this
        }
        let factor = 1 / l
        for (let key of Object.keys(this)) {
            this[key] = this[key] * factor
        }
        return this
    }

    dot(v) {
        return this.x * v.x + this.y * v.y + this.z * v.z
    }

    cross(v) {
        let x = this.y * v.z - this.z * v.y
        let y = this.z * v.x - this.x * v.z
        let z = this.x * v.y - this.y * v.x
        return GuaVector.new(x, y, z)
    }
}

// module.exports = GuaVector
