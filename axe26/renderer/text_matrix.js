// const GuaVector = require('./vector')
// const Matrix = require('./matrix')

class TestMatrix {
    constructor() {
    }
    test() {
        this.testMatrixLookAtLH()
        this.testMatrixRotation()
        this.testMatrixTransform()
    }
    ensure(condition, message) {
        // 如果 condition 为 false，输出 message
        if (!condition) {
            console.error('测试有误',message)
        }
    }
    floatEqual(a, b) {
        // 浮点数不能直接比较，一般用这样的方式来判断
        return a - b <= 0.0001
    }
    matrixEqual(m1, m2) {
        // 判断 2 个 GuaMatrix 是否相等
        for (let i = 0; i < m1.length; i++) {
            for (let j = 0; j < m2.length; j++) {
                if (!this.floatEqual(m1.m, m2.m)) {
                    return false
                }
            }
        }
        return true
    }
    vectorEqual(v1, v2) {
        // 判断 2 个 GuaGuaVector 是否相等
        for (let key of Object.keys(v1)) {
            if (!this.floatEqual(v1[key], v2[key])) {
                return false
            }
        }
        return true
    }
    testMatrixLookAtLH() {
        let cameraPosition = GuaVector.new(0, 0, 10)
        let cameraTarget = GuaVector.new(0, 0, 0)
        let cameraUp = GuaVector.new(0, 1, 0)
        let matrix = Matrix.lookAtLH(cameraPosition, cameraTarget, cameraUp)
        let values = [
            [-1, 0, 0, 0],
            [0, 1, 0, 0],
            [0, 0, -1, 0],
            [0, 0, 10, 1],
        ]
        this.ensure(this.matrixEqual(matrix, Matrix.new(values)), 'testMatrixLookAtLH')
    }
    testMatrixRotation() {
        let v = GuaVector.new(10, 20, 30)
        let matrix = Matrix.rotation(v)
        let values = [
            [0.554, 0.829, 0.079, 0.000],
            [0.327, -0.129, -0.936, 0.000],
            [-0.766, 0.544, -0.342, 0.000],
            [0.000, 0.000, 0.000, 1.000],
        ]
        this.ensure(this.matrixEqual(matrix, Matrix.new(values)), 'testMatrixRotation')
    }
    testMatrixTransform() {
        let v = GuaVector.new(0.593800, -0.147900, 0.143700)
        let values = [
            [-1.774, 0.000, 0.010, 0.010],
            [0.000, 2.365, 0.000, 0.000],
            [-0.018, 0.000, -1.010, -1.000],
            [0.000, 0.000, 10.091, 10.000],
        ]
        let vector = GuaVector.new(-0.107060, -0.035470, 1.009077)
        log(Matrix.new(values))
        this.ensure(this.vectorEqual(vector, Matrix.new(values).transformVector(v)), 'testMatrixTransform')
    }
}

let tm = new TestMatrix()
tm.test()
