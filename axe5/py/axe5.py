from operator import add, sub, mul, truediv

def tokens(code):
    """
    code 是 str, 例子如下
    code = '+ - [] 123 gua axe4'
    返回 ['+', '-', '[', ']', '123', 'gua', 'axe4']
    """
    code = code.split()
    res = []
    def isWord(word):
        result = True
        for w in word:
            for y in w:
                if not(ord(y) in range(48, 58) or ord(y) in range(65, 91) or ord(y) in range(97, 123)):
                    return False
        return result

    for x in code:
        if isWord(x):
            res += [x]
        else:
            res += list(x)
    return res


def apply(tokens):
    """
    tokens 是一个数组
    第一个元素是 '+' '-' '*' '/'
    剩下的元素都是数字
    返回计算结果
    """
    def reduce(fn, l):
        reduced = l[0]
        rest = l[1:]
        for x in rest:
            reduced = fn(reduced, x)
        return reduced
    first = tokens[0]
    rest = tokens[1:]
    if first == "+":
        return sum(rest)
    if first == "-":
        return reduce(sub, rest)
    if first == "*":
        return reduce(mul, rest)
    if first == "/":
        return reduce(truediv, rest)

def ensure(condition, message):
    if not condition:
        print("测试失败", message)

def testTokens():
    ensure(tokens('+ - [] 123 gua axe4') == ['+', '-', '[', ']', '123', 'gua', 'axe4'], "test tokens 1")
    ensure(tokens('cool + + *** 123 gua axe4') == ['cool', '+', '+', '*', '*', '*', '123', 'gua', 'axe4'], "test tokens 2")
    ensure(tokens('aa a123 b123 123 gua å∫ç') == ['aa', 'a123', 'b123', '123', 'gua', 'å', '∫', 'ç'], "test tokens 3")

def testApply():
    ensure(apply(['+', 1, 2, 3]) == 6, "test apply 1")
    ensure(apply(['-', 10, 2, 3]) == 5, "test apply 2")
    ensure(apply(['*', 1, 2, 6]) == 12, "test apply 3")
    ensure(apply(['/', 6, 2, 3]) == 1, "test apply 4")

def __main():
    testTokens()
    testApply()

__main()