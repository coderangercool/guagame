#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>
#include<string.h>
#include<semaphore.h>
#include "thread_safe_stack.h"

typedef struct node {
    int member;
    struct node *pNext;
}Node, *pNode;

typedef struct stack{
    pNode top;
    pNode bottom;
    int size;
}Stack, *pStack;

static Stack s;
static sem_t sem;

void
initStack(pStack ps, int size) {
    ps->top = (pNode) malloc(sizeof(Node));
    ps->bottom = ps->top;
    ps->top->pNext = NULL;
    ps->size = size;
}

void
push(pStack ps, int data) {
    pNode pNew = (pNode) malloc(sizeof(Node));
    pNew->member = data;
    pNew->pNext = ps->top;
    ps->top = pNew;
}

int
pop(pStack ps) {
    pNode pSwap = NULL;
    int returnVal;
    returnVal = ps->top->member;
    pSwap = ps->top;
    ps->top = ps->top->pNext;
    free(pSwap);
    return returnVal;
}

void *
pushS() {
    sem_wait(&sem);
    push(&s, 1);
    sem_post(&sem);
    return NULL;
}

void *
popS() {
    sem_wait(&sem);
    pop(&s);
    sem_post(&sem);
    return NULL;
}

void
multiThread(void) {
    int n = 10;
    int m = 10;
    pthread_t tid1[n];
    pthread_t tid2[n];
    for (int i = 0; i < n; i++) {
        for (int j = m; j < m; j++) {
            pthread_create(&tid1[i], NULL, pushS, NULL);
        }
        for (int j = m; j < m; j++) {
            pthread_create(&tid2[i], NULL, popS, NULL);
        }
    }
    for (int i = 0; i < n; i++) {
        for (int j = m; j < m; j++) {
            pthread_join(tid1[i], NULL);
        }
        for (int j = m; j < m; j++) {
            pthread_join(tid2[i], NULL);
        }
    }
}

int
main(void) {
    //    pthread_mutex_init(&m, NULL);
    //    sem_init(&sem, 0, 1);
    initStack(&s, 100);
    multiThread();
    
//    printf("main end, %d\n", balance);
    printf("stack size %d\n", s.size);
    return 0;
}
