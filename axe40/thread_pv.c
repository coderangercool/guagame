#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>
#include<string.h>
#include<semaphore.h>

static int balance = 0;
//static pthread_mutex_t m;
static sem_t sem;

void *
deposit(void *args) {
//    pthread_mutex_lock(&m);
    sem_wait(&sem);
    balance += 10;
//    pthread_mutex_unlock(&m);
    sem_post(&sem);
    return NULL;
}

void *
withdraw(void *args) {
//    pthread_mutex_lock(&m);
    sem_wait(&sem);
    balance -= 10;
//    pthread_mutex_unlock(&m);
    sem_post(&sem);
    return NULL;
}

void
multiThread(void) {
    int n = 100;
    pthread_t tid1[n];
    pthread_t tid2[n];
    for (int i = 0; i < n; i++) {
        pthread_create(&tid1[i], NULL, deposit, NULL);
        pthread_create(&tid2[i], NULL, withdraw, NULL);
    }
    for (int i = 0; i < n; i++) {
        pthread_join(tid1[i], NULL);
        pthread_join(tid2[i], NULL);
    }
}

int
main(void) {
//    pthread_mutex_init(&m, NULL);
//    sem_init(&sem, 0, 1);
    multiThread();
    
    printf("main end, %d\n", balance);
    
    return 0;
}

